/*
 * git sequencer builtin command
 *
 * Copyright (C) 2008-2009 Stephan Beyer
 */
#include "builtin.h"
#include "cache.h"
#include "cache-tree.h"
#include "utf8.h"
#include "dir.h"
#include "parse-options.h"
#include "refs.h"
#include "run-command.h"
#include "string-list.h"
#include "strbuf.h"
#include "pick.h"
#include "rerere.h"
#include "diff.h"
#include "revision.h"
#include "unpack-trees.h"
#include "merge-recursive.h"

#define SEQ_DIR "sequencer"
#define SEQ_MARK "refs/sequencer-marks"

#define TODO_FILE	git_path(SEQ_DIR "/todo")
#define DONE_FILE	git_path(SEQ_DIR "/done")
#define SAVE_FILE	git_path(SEQ_DIR "/save")
#define DESTINY_FILE	git_path(SEQ_DIR "/destiny")
#define PATCH_FILE	git_path(SEQ_DIR "/patch")
#define MESSAGE_FILE	git_path(SEQ_DIR "/message")
#define MERGE_HEAD	git_path("MERGE_HEAD")
#define MERGE_MSG	git_path("MERGE_MSG")
#define COMMIT_EDITMSG	git_path("COMMIT_EDITMSG")
#define SQUASH_MSG	git_path("SQUASH_MSG")

/**********************************************************************
 * Data structures
 */

struct user_info {
	const char *name;
	const char *mail;
	const char *time; /* "<timestamp> <timezone>" */
};

struct commit_info {
	struct user_info author; /* author info */
	struct user_info committer; /* not used, but for easy extendability */
	const char *encoding; /* encoding */
	char *subject; /* basically the first line of the summary */
	struct strbuf summary; /* the commit message */
	char *source; /* source of the commit message, either
		       * "message", "merge", "squash" or a commit SHA1 */
	char *patch; /* a patch */
	struct string_list parents; /* list of parents' hex'ed sha1 ids */
};

/* A structure for a parsed instruction line plus a next pointer
 * to allow linked list behavior */
struct parsed_insn {
	int argc;
	const char **argv;
	int line;
	struct strbuf orig;
	struct parsed_insn *next;
};

struct parsed_file {
	size_t count;
	size_t total;
	struct parsed_insn *first;
	struct parsed_insn *last;
	struct parsed_insn *cur; /* a versatile helper */
};

struct instruction {
	const char * const *usage;
	struct option *options;
	void (*init)(void);
	int (*check)(int, const char **);
	int (*act)(int, const char **);
};

#define INSTRUCTION_NO_OPTS(name) \
	{ insn_ ## name ## _usage, \
	  no_insn_options, \
	  NULL, \
	  insn_ ## name ## _check, \
	  insn_ ## name ## _act }

#define INSTRUCTION(name) \
	{ insn_ ## name ## _usage, \
	  insn_ ## name ## _options, \
	  insn_ ## name ## _init, \
	  insn_ ## name ## _check, \
	  insn_ ## name ## _act }


/**********************************************************************
 * Global variables
 */

static char *reflog;

static const char *caller_abort, *caller_continue, *caller_skip;
static int caller_check_failed = 0;

static struct string_list available_marks;
static int marks_total;
/* This serves sanity checking purposes, but also need to be *written*
 * on insn acting. */

static int squash_count = 0;

static int die_with_merges = 0;

#define ACTION_ABORT    1
#define ACTION_CONTINUE 2
#define ACTION_SKIP     4
#define ACTION_STATUS   8
#define ACTION_EDIT    16

static int allow_dirty = 0, batchmode = 0, verbosity = 1, advice = 1;

static struct parsed_file contents;
static int todo_line;

static const char *skiphead = NULL;
static const char *orig_headname = NULL;
static unsigned char orig_head_sha1[20];
static unsigned char head_sha1[20];
static unsigned char paused_at_sha1[20];
static enum {
	WHY_UNKNOWN,
	WHY_TODO,
	WHY_CONFLICT,
	WHY_PAUSE,
	WHY_RUN
} why;

#define OPT_GENERAL_OPTIONS \
	OPT_STRING(0, "author", &opt.author, "author", "override author"), \
	OPT_STRING('C', "reuse-commit", &opt.reuse_commit, "commit", \
		"reuse message and authorship data from commit"), \
	OPT_STRING('F', "file", &opt.file, "file", \
		"take commit message from given file"), \
	OPT_STRING('m', "message", &opt.message, "msg", \
		"specify commit message"), \
	OPT_STRING('M', "reuse-message", &opt.reuse_message, "commit", \
		"reuse message from commit"), \
	OPT_BOOLEAN(0, "signoff", &opt.signoff, "add signoff"), \
	OPT_BOOLEAN('e', "edit", &opt.edit, \
		"invoke editor to edit commit message")

static struct {
	char *author;
	char *reuse_commit;
	char *reuse_message;
	char *file;
	char *message;
	int signoff;
	int edit;
} opt;

static struct {
	int standard;
	char *strategy;
} merge_opt;

static struct {
	int threeway;
	int keep;
	int noutf8;
	struct string_list apply;
} patch_opt;

static struct {
	int reverse;
	int mainline;
} pick_opt;

static struct {
	char *dir;
} run_opt;

static int squash_opt;
#define OPT_SQUASH_FROM           1
#define OPT_SQUASH_INCLUDE_MERGES 2

static struct commit_info next_commit;

/* Some forward declaration */
static int find_insn(const char *);


/**********************************************************************
 * Cleanup routines
 */

static void free_commit_info(struct commit_info *info)
{
	/* We have to take care that the components are set to NULL
	 * after freeing, so that we can check if they are set or not. */
	if (info->encoding) {
		free((void *)info->encoding);
		info->encoding = NULL;
	}
	if (info->subject) {
		free(info->subject);
		info->subject = NULL;
	}
	strbuf_release(&info->summary);
	if (info->patch) {
		free(info->patch);
		info->patch = NULL;
	}
	if (info->source) {
		free(info->source);
		info->source = NULL;
	}
	if (info->author.name) {
		free((void *)info->author.name);
		info->author.name = NULL;
	}
	if (info->author.mail) {
		free((void *)info->author.mail);
		info->author.mail = NULL;
	}
	if (info->author.time) {
		free((void *)info->author.time);
		info->author.time = NULL;
	}
	if (info->committer.name) {
		free((void *)info->committer.name);
		info->committer.name = NULL;
	}
	if (info->committer.mail) {
		free((void *)info->committer.mail);
		info->committer.mail = NULL;
	}
	if (info->committer.time) {
		free((void *)info->committer.time);
		info->committer.time = NULL;
	}
	info->parents.strdup_strings = 1;
	string_list_clear(&info->parents, 0);
	info->parents.strdup_strings = 0;
}

/* Remove a parsed insn replace it by the next one */
static void free_parsed_insn(struct parsed_insn **p)
{
	int i;
	struct parsed_insn *cur = *p;
	*p = cur->next;
	for (i = 0; i < cur->argc; ++i)
		free((void *)cur->argv[i]);
	free(cur->argv);
	strbuf_release(&cur->orig);
	free(cur);
}

static void free_available_marks(void)
{
	available_marks.nr = marks_total;
	available_marks.strdup_strings = 1; /* free items */
	string_list_clear(&available_marks, 0);
	marks_total = 0;
}

static void small_cleanup(void)
{
	struct parsed_insn *cur;
	/* free parsed_insns of contents */
	for (cur = contents.first; cur; free_parsed_insn(&cur));

	/* free global stuff */
	free_commit_info(&next_commit);
	free_available_marks();
}

/* A callback function for for_each_ref() to get a list of marks */
static int mark_list(const char *ref, const unsigned char *sha, int flags, void *data)
{
	if (!prefixcmp(ref, SEQ_MARK)) {
		struct string_list marks = *(struct string_list *)data;
		string_list_append(ref, &marks);
		marks.items[marks.nr - 1].util = xmalloc(20);
		hashcpy(marks.items[marks.nr - 1].util, sha);
		*(struct string_list *)data = marks;
	}
	return 0;
}

static void cleanup(void)
{
	struct string_list marks;
	struct strbuf seq_dir = STRBUF_INIT;
	int i;
	/* remove all marks */
	memset(&marks, 0, sizeof(struct string_list));
	marks.strdup_strings = 1;
	for_each_ref(mark_list, &marks);
	for (i = 0; i < marks.nr; ++i)
		delete_ref(marks.items[i].string, marks.items[i].util, 0);
	string_list_clear(&marks, 1);

	small_cleanup();

	strbuf_addstr(&seq_dir, git_path(SEQ_DIR));
	remove_dir_recursively(&seq_dir, 0);
	strbuf_release(&seq_dir);
}


/**********************************************************************
 * Miscellaneous helper functions
 */

static void print_advice(void)
{
	if (!advice)
		return;
	switch(why) {
	case WHY_CONFLICT:
		if (verbosity && next_commit.subject)
			printf("\nConflict at: %s\n", next_commit.subject);
		printf("\n"
		"After resolving the conflicts, mark the corrected paths with"
		"\n\n\tgit add <paths>\n\n"
		"and run\n\n\t%s\n\n"
		"Note, that your working tree must match the index.\n",
			caller_continue);
		break;
	case WHY_PAUSE:
		if (verbosity && next_commit.subject)
			printf("Pause at: %s\n", next_commit.subject);
		printf("\nYou can now edit files and add them to the index.\n"
		"Once you are satisfied with your changes, run\n\n\t%s\n\n"
		"If you only want to change the commit message, run\n"
		"git commit --amend before.\n", caller_continue);
		break;
	case WHY_RUN:
		if (verbosity && next_commit.subject)
			printf("\nRunning failed at:\n\t%s\n",
							next_commit.subject);
		printf("You can now fix the problem.  When manual runs pass,\n"
			"add the changes to the index and invoke\n\n\t%s\n",
							caller_continue);
		break;
	case WHY_TODO:
		printf("\nFix with git sequencer --edit or abort with %s.\n",
			caller_abort);
		break;
	default:
		break;
	}
}

static int parse_and_init_tree_desc(const unsigned char *sha1,
						struct tree_desc *desc)
{
	struct tree *tree = parse_tree_indirect(sha1);
	if (!tree)
		return 1;
	init_tree_desc(desc, tree->buffer, tree->size);
	return 0;
}

static int reset_index_file(const unsigned char *sha1, int update, int dirty)
{
	int nr = 1;
	int newfd;
	struct tree_desc desc[2];
	struct unpack_trees_options opts;
	struct lock_file *lock = xcalloc(1, sizeof(struct lock_file));

	memset(&opts, 0, sizeof(opts));
	opts.head_idx = 1;
	opts.src_index = &the_index;
	opts.dst_index = &the_index;
	opts.reset = 1; /* ignore unmerged entries and overwrite wt files */
	opts.merge = 1;
	opts.fn = oneway_merge;
	if (verbosity > 2)
		opts.verbose_update = 1;
	if (update) /* update working tree */
		opts.update = 1;

	newfd = hold_locked_index(lock, 1);

	read_cache_unmerged();

	if (dirty) {
		if (get_sha1("HEAD", head_sha1))
			return error("You do not have a valid HEAD.");
		if (parse_and_init_tree_desc(head_sha1, desc))
			return error("Failed to find tree of HEAD.");
		nr++;
		opts.fn = twoway_merge;
	}

	if (parse_and_init_tree_desc(sha1, desc + nr - 1))
		return error("Failed to find tree of %s.", sha1_to_hex(sha1));
	if (unpack_trees(nr, desc, &opts))
		return -1;
	if (write_cache(newfd, active_cache, active_nr) ||
	    commit_locked_index(lock))
		return error("Could not write new index file.");

	return 0;
}

/*
 * Realize reset --hard behavior.
 * If allow_dirty is set and there is a dirty working tree,
 * then the changes are to be kept.
 */
static int reset_almost_hard(unsigned const char sha[20])
{
	int err = allow_dirty ?
		(reset_index_file(sha, 1, 1) || reset_index_file(sha, 0, 0)) :
		reset_index_file(sha, 1, 0);
	if (err)
		return error("Could not reset index.");

	return update_ref(reflog, "HEAD", sha, NULL, 0, MSG_ON_ERR);
}

/* rerere clear functionality */
static void rerere_clear(void) {
	const char *args[] = {
		"rerere",
		"clear",
		NULL
	};
	run_command_v_opt(args, RUN_GIT_CMD | RUN_COMMAND_NO_STDIN);
}

/* Generate purely informational patch file */
static void make_patch(struct commit *commit)
{
	struct commit_list *parents = commit->parents;
	const char **args;
	struct child_process chld;
	int fd = open(PATCH_FILE, O_WRONLY | O_CREAT, 0666);
	if (fd < 0)
		return;

	memset(&chld, 0, sizeof(chld));
	if (!parents) {
		write(fd, "Root commit\n", 12);
		close(fd);
		return;
	} else if (!parents->next) {
		args = xcalloc(5, sizeof(char *));
		args[0] = "diff-tree";
		args[1] = "-p";
		args[2] = xstrdup(sha1_to_hex(parents->item->object.sha1));
		args[3] = xstrdup(sha1_to_hex(((struct object *)commit)->sha1));
	} else {
		int i = 0;
		int count = 1;

		for (; parents; parents = parents->next)
			++count;
		args = xcalloc(count + 3, sizeof(char *));
		args[i++] = "diff";
		args[i++] = "--cc";
		args[i++] = xstrdup(sha1_to_hex(commit->object.sha1));

		for (parents = commit->parents; parents;
		     parents = parents->next)
			args[i++] = xstrdup(sha1_to_hex(
					    parents->item->object.sha1));
	}

	chld.argv = args;
	chld.git_cmd = 1;
	chld.out = fd;

	/* Run, ignore errors. */
	if (start_command(&chld))
		return;
	finish_command(&chld);

	/* TODO: free dup'ed SHAs in argument list */
}

/* Commit current index with information next_commit onto parent_sha1. */
static int do_commit(unsigned char *parent_sha1)
{
	int failed;
	unsigned char tree_sha1[20];
	unsigned char commit_sha1[20];
	struct strbuf sbuf;
	const char *reencoded = NULL;

	if (squash_count) {
		squash_count = 0;
		if (file_exists(SQUASH_MSG))
			unlink(SQUASH_MSG);
	}

	if (!index_differs_from("HEAD", 0) &&
	    !next_commit.parents.nr)
		return error("No changes! Do you really want an empty commit?");

	if (!next_commit.author.name || !next_commit.author.mail)
		return error("Internal error: Author information not set properly.");

	if (write_cache_as_tree(tree_sha1, 0, NULL))
		return 1;

	if (!next_commit.encoding)
		next_commit.encoding = xstrdup("utf-8");
	if (!git_commit_encoding)
		git_commit_encoding = "utf-8";

	strbuf_init(&sbuf, 8192); /* should avoid reallocs for the headers */
	strbuf_addf(&sbuf, "tree %s\n", sha1_to_hex(tree_sha1));
	if (parent_sha1)
		strbuf_addf(&sbuf, "parent %s\n", sha1_to_hex(parent_sha1));
	if (next_commit.parents.nr) {
		int i;
		for (i = 0; i < next_commit.parents.nr; ++i)
			strbuf_addf(&sbuf, "parent %s\n",
					next_commit.parents.items[i].string);
	}
	if (!next_commit.author.time) {
		char time[50];
		datestamp(time, sizeof(time));
		next_commit.author.time = xstrdup(time);
	}

	stripspace(&next_commit.summary, 1);

	/* if encodings differ, reencode whole buffer */
	if (strcasecmp(git_commit_encoding, next_commit.encoding)) {
		if ((reencoded = reencode_string(next_commit.author.name,
				git_commit_encoding, next_commit.encoding))) {
			free((void *)next_commit.author.name);
			next_commit.author.name = reencoded;
		}
		if ((reencoded = reencode_string(next_commit.summary.buf,
				git_commit_encoding, next_commit.encoding))) {
			strbuf_reset(&next_commit.summary);
			strbuf_addstr(&next_commit.summary, reencoded);
		}
	}
	strbuf_addf(&sbuf, "author %s <%s> %s\n", next_commit.author.name,
			next_commit.author.mail, next_commit.author.time);
	strbuf_addf(&sbuf, "committer %s\n", git_committer_info(0));
	if (!is_encoding_utf8(git_commit_encoding))
		strbuf_addf(&sbuf, "encoding %s\n", git_commit_encoding);
	strbuf_addch(&sbuf, '\n');
	strbuf_addbuf(&sbuf, &next_commit.summary);
	if (sbuf.buf[sbuf.len-1] != '\n')
		strbuf_addch(&sbuf, '\n');

	failed = write_sha1_file(sbuf.buf, sbuf.len, commit_type, commit_sha1);
	strbuf_release(&sbuf);
	if (failed)
		return 1;

	if (verbosity > 1)
		printf("Created %scommit %s\n",
			parent_sha1 || next_commit.parents.nr ? "" : "initial ",
			sha1_to_hex(commit_sha1));

	if (update_ref(reflog, "HEAD", commit_sha1, NULL, 0, 0))
		return error("Could not update HEAD to %s.",
						sha1_to_hex(commit_sha1));
	if (next_commit.parents.nr && available_marks.nr < marks_total &&
	    available_marks.items[available_marks.nr].string[0] == 'm')
		++available_marks.nr;
	return 0;
}

static int do_commit_with_msgfile(unsigned char *parent_sha1)
{
	strbuf_release(&next_commit.summary);
	if (strbuf_read_file(&next_commit.summary, MESSAGE_FILE, 0) < 0)
		return error("Could not read message file %s: %s.",
					MESSAGE_FILE, strerror(errno));
	stripspace(&next_commit.summary, 1);
	if (!next_commit.summary.len)
		return error("Commit message in %s is empty!", MESSAGE_FILE);
	return do_commit(parent_sha1);
}

static int do_merge(void)
{
	int i = 0;
	int j;
	const char **args;

	args = xcalloc(next_commit.parents.nr + 5, sizeof(char *));
	args[i++] = "merge";
	args[i++] = "--no-commit";
	if (merge_opt.strategy) {
		args[i++] = "-s";
		args[i++] = merge_opt.strategy;
	}
	if (next_commit.parents.nr) {
		for (j = 0; j < next_commit.parents.nr; ++j)
			args[i++] = next_commit.parents.items[j].string;
	} else {
		free(args);
		return error("No parents specified.");
	}
	args[i++] = NULL;
	j = run_command_v_opt(args, RUN_GIT_CMD | RUN_COMMAND_NO_STDIN);
	free(args);
	discard_cache();
	return j;
}

/*
 * Fill next_commit.author according to ident.
 * Ident may have one of the following forms:
 * 	"name <e-mail> timestamp timezone\n..."
 * 	"name <e-mail> timestamp timezone"
 * 	"name <e-mail>"
 */
static void set_author_info(const char *ident)
{
	const char *tmp1 = strstr(ident, " <");
	const char *tmp2;
	char *data;
	if (!tmp1)
		return;
	tmp2 = strstr(tmp1+2, ">");
	if (!tmp2)
		return;
	if (tmp2[1] != 0 && tmp2[1] != ' ')
		return;

	data = xmalloc(strlen(ident)); /* a trivial upper bound */

	snprintf(data, tmp1-ident+1, "%s", ident);
	next_commit.author.name = xstrdup(data);
	snprintf(data, tmp2-tmp1-1, "%s", tmp1+2);
	next_commit.author.mail = xstrdup(data);

	if (tmp2[1] == 0) {
		free(data);
		return;
	}

	tmp1 = strpbrk(tmp2+2, "\r\n");
	if (!tmp1)
		tmp1 = tmp2 + strlen(tmp2);

	snprintf(data, tmp1-tmp2-1, "%s", tmp2+2);
	next_commit.author.time = xstrdup(data);
	free(data);
}

static void reset_next_commit(void)
{
	const char *ident = git_author_info(IDENT_ERROR_ON_NO_NAME);
	memset(&next_commit, 0, sizeof(next_commit));
	strbuf_init(&next_commit.summary, 0);
	set_author_info(ident);
}

static void set_message_source(const char *source)
{
	if (next_commit.source)
		free(next_commit.source);
	next_commit.source = xstrdup(source);
}

/* Fill next_commit from commit header data (without commit summary),
 * and return a pointer to the commit summary. */
static const char *get_commit_header(struct commit *commit, int get_parents)
{
	const char *p = commit->buffer, *eol;
	/* go through commit header */
	while (*p && *p != '\n') {
		eol = strchrnul(p + 1, '\n');
		/* check for interesting headers */
		if (!prefixcmp(p, "author "))
			set_author_info(p + 7);
		else if (!prefixcmp(p, "encoding "))
			next_commit.encoding = xstrndup(p + 9, eol - p - 9);
		else if (get_parents && !prefixcmp(p, "parent "))
			string_list_append(xstrndup(p + 7, 40),
							&next_commit.parents);
		p = eol;
		if (*p == '\n')
			++p;
	}
	return p;
}

/* Fill next_commit from commit data */
static void get_commit_info(struct commit *commit, int get_parents)
{
	const char *p = get_commit_header(commit, get_parents);
	if (*p)
		strbuf_addstr(&next_commit.summary, p + 1);
	set_message_source(sha1_to_hex(((struct object *)commit)->sha1));
}

/* Set subject, an information for the case of conflict */
static void set_pick_subject(const char *hex, struct commit *commit)
{
	const char *tmp = strstr(commit->buffer, "\n\n");
	if (tmp) {
		const char *eol;
		int len = strlen(hex);
		tmp += 2;
		eol = strchrnul(tmp, '\n');
		next_commit.subject = xmalloc(eol - tmp + len + 5);
		snprintf(next_commit.subject, eol - tmp + len + 5, "%s... %s",
								hex, tmp);
	}
}

/* Return a commit object of "arg" */
static struct commit *get_commit(const char *arg)
{
	unsigned char sha1[20];

	if (get_sha1(arg, sha1)) {
		error("Could not find '%s'", arg);
		return NULL;
	}
	return lookup_commit_reference(sha1);
}

static int fast_forward_possible(struct commit *commit)
{
	struct commit_list *parents = commit->parents;
	int i = 0;

	/* check if first parent equal to HEAD */
	if (!parents || hashcmp(head_sha1, parents->item->object.sha1))
		return 0; /* not equal */

	/* no other parents there? */
	if (!parents->next && !next_commit.parents.nr)
		return 1;

	/* check if each further parent is equal */
	while (parents->next) {
		parents = parents->next;
		if (i < next_commit.parents.nr) {
			unsigned char sha[20];
			if (get_sha1_hex(next_commit.parents.items[i].string,
									sha))
				return 0; /* no hex value */
			if (hashcmp(sha, parents->item->object.sha1))
				return 0; /* not equal */
		} else
			return 0; /* not equal number of parents */
		++i;
	}
	if (i < next_commit.parents.nr)
		return 0; /* next commit will have further parents */
	return 1; /* possible */
}

static int do_fast_forward(const unsigned char *sha)
{
	if (reset_almost_hard(sha))
		return error("Fast-forwarding %s failed.",
					sha1_to_hex(sha));
	if (verbosity > 1)
		printf("Fast-forwarded %s\n",
					sha1_to_hex(sha));
	return 0;
}

static const char *resolve_symbolic_ref(const char *symref)
{
	unsigned char dummy[20];
	int flag;
	const char *ref = resolve_ref(symref, dummy, 0, &flag);
	return (flag & REF_ISSYMREF) ? ref : NULL;
}

/*
 * The --caller feature is for user scripts only. (Hence undocumented.)
 * User scripts should pass an argument like:
 * --caller="git foo|abrt|go|next"
 * on every git sequencer call. (It is only ignored on
 * --edit and --status.)
 * So git sequencer knows that
 * "git foo abrt" will abort,
 * "git foo go" will continue and
 * "git foo next" will skip the sequencing process.
 * This is useful if your user script does some extra
 * preparations or cleanup before/after calling
 *   git sequencer --caller="..." --abort|--continue|--skip
 *
 * Running git-sequencer without the same --caller string
 * fails then, until the sequencing process has finished or
 * aborted.
 *
 * Btw, --caller="my_tiny_script.sh|-a||" will be
 * interpreted, that users must invoke "my_tiny_script.sh -a"
 * to abort, but can invoke "git sequencer --continue" and
 * "git sequencer --skip" to continue or skip.
 * And it is also possible to provide three different scripts
 * by --caller="|script 1|tool 2|util 3".
 *
 * If your user script does not need any special
 * abort/continue/skip behavior, then just do NOT pass
 * the --caller option.
 */
static int prepare_caller_strings(const struct option *opt, const char *arg,
				  int unset)
{
	struct strbuf **what;
	char const **to[] = {
		&caller_abort, &caller_continue, &caller_skip
	};
	struct strbuf arg_buf = STRBUF_INIT;
	int len;
	int i;

	/* reset */
	caller_abort = "git sequencer --abort";
	caller_continue = "git sequencer --continue";
	caller_skip = "git sequencer --skip";
	if (unset) /* --no-caller: use reset caller */
		return 0;

	len = strlen(arg);
	strbuf_add(&arg_buf, arg, len);
	what = strbuf_split(&arg_buf, '|');
	if (what[0]->len)
		what[0]->buf[what[0]->len-1] = ' ';
	for (i = 1; i < 4; ++i) {
		if (!what[i])
			return error("Wrong --caller format.");
		if (i != 3) /* remove '|' delimiter in field 1 and 2 */
			what[i]->len--;
		if (!what[i]->len) /* keep the reset value if empty */
			continue;
		strbuf_insert(what[i], 0, what[0]->buf, what[0]->len);
		*to[i - 1] = strbuf_detach(what[i], NULL);
	}
	strbuf_list_free(what);
	strbuf_release(&arg_buf);
	return 0;
}

/* Generate a real ref from :mark. ref is limited to 50 bytes. */
static int mark_to_ref(const char *arg, char *ref, int allow_uncoloned) {
	if (!arg)
		return 1;
	if (arg[0] == ':')
		snprintf(ref, 50, SEQ_MARK "/%d", atoi(arg + 1));
	else if (allow_uncoloned)
		snprintf(ref, 50, SEQ_MARK "/%d", atoi(arg));
	else
		return 1;
	return 0;
}

/* Like get_sha1(), but also takes :mark as argument */
static int mark_to_sha1(const char *arg, unsigned char *sha1) {
	char ref[50];
	const char *x = ref;
	if (mark_to_ref(arg, ref, 0))
		x = arg;
	if (get_sha1(x, sha1))
		return 1;
	return 0;
}

/* Is next_commit.summary empty? Ignore comments and Signed-off-by lines */
static int message_is_empty(void)
{
	char *tmp;
	stripspace(&next_commit.summary, 1);
	if (!next_commit.summary.len)
		return 1;
	tmp = next_commit.summary.buf;
	/* Ignore newlines and Signed-off-by: lines */
	while (*tmp) {
		while (*tmp++ == '\n');
		if (!*tmp)
			return 1;
		if (!prefixcmp(tmp, "Signed-off-by:")) {
			tmp = strchr(tmp + 14, '\n');
			if (!tmp)
				return 1;
			++tmp;
		} else
			return 0;
	}
	return 0;
}

static int set_verbosity(int verbose)
{
	char tmp[] = "0";
	verbosity = verbose;
	if (verbosity <= 0) {
		verbosity = 0;
		advice = 0;
	} else if (verbosity > 5)
		verbosity = 5;
	/* Git does not run on EBCDIC, so we rely on ASCII: */
	tmp[0] += verbosity;
	setenv("GIT_MERGE_VERBOSITY", tmp, 1);
	return 0;
}


/**********************************************************************
 * Functions that deal with saving and loading
 */

static int write_commit_summary_into(const char *filename)
{
	struct lock_file *lock = xcalloc(1, sizeof(struct lock_file));
	int fd = hold_lock_file_for_update(lock, filename, 0);
	if (fd < 0)
		return error("Unable to create '%s.lock': %s", filename,
							strerror(errno));
	if (write_in_full(fd, next_commit.summary.buf,
			      next_commit.summary.len) < 0)
		return error("Could not write to %s: %s",
						filename, strerror(errno));
	if (commit_lock_file(lock) < 0)
		return error("Error wrapping up %s", filename);
	return 0;
}

/* Run the prepare-commit-msg hook on COMMIT_EDITMSG. */
static int prepare_commit_msg_hook(void)
{
	const char *hook_arg1 = next_commit.source;
	const char *hook_arg2 = NULL;

	if (hook_arg1 &&
	    strcmp(hook_arg1, "message") &&
	    strcmp(hook_arg1, "template") &&
	    strcmp(hook_arg1, "merge") &&
	    strcmp(hook_arg1, "squash"))
	{
		hook_arg2 = hook_arg1;
		hook_arg1 = "commit";
	}

	return run_hook(NULL, "prepare-commit-msg", COMMIT_EDITMSG,
			hook_arg1, hook_arg2, NULL);
}

/* Copy file with mode 0666. If destination exists, unlink it before. */
static int copy_file_overwrite(const char *dst, const char *src)
{
	if (file_exists(dst))
		unlink(dst);
	return copy_file(dst, src, 0666);
}

/* Edit commit message in COMMIT_EDITMSG. */
static int edit_message(int from_buf)
{
	if (from_buf) {
		if (write_commit_summary_into(COMMIT_EDITMSG))
			return -1;
	} else {
		char *tmp = xstrdup(COMMIT_EDITMSG);
		if (copy_file_overwrite(tmp, MESSAGE_FILE)) {
			error("Could not copy '%s' to '%s'.",
						MESSAGE_FILE, tmp);
			free(tmp);
			return -1;
		}
		free(tmp);
	}

	/* Run prepare-commit-msg hook and editor, ... */
	if (prepare_commit_msg_hook() ||
	    launch_editor(COMMIT_EDITMSG, NULL, NULL))
		return -1;

	/* ...reread, ... */
	strbuf_release(&next_commit.summary);
	if (strbuf_read_file(&next_commit.summary, COMMIT_EDITMSG, 0) < 0)
		return error("Could not read message file %s.", COMMIT_EDITMSG);
	if (message_is_empty())
		return error("No commit message given.");

	/* ...and copy to our general message file. */
	return write_commit_summary_into(MESSAGE_FILE);
}

/* Save a string that can be worthy after a pause. */
static int save(const char *name, const char *value)
{
	int ret;
	config_exclusive_filename = SAVE_FILE;
	ret = git_config_set(name, value);
	config_exclusive_filename = NULL;
	return ret;
}

/* Save an integer that can be worthy after a pause. */
static int save_int(const char *name, ssize_t value)
{
	char tmp[50];
	snprintf(tmp, sizeof(tmp), "%zi", value);
	return save(name, tmp);
}

static int save_commit_data(void)
{
	char *msgfile;
	if (next_commit.author.name)
		save("sequencer.author.name", next_commit.author.name);
	if (next_commit.author.mail)
		save("sequencer.author.mail", next_commit.author.mail);
	if (next_commit.author.time)
		save("sequencer.author.time", next_commit.author.time);
	if (next_commit.encoding)
		save("sequencer.encoding", next_commit.encoding);
	if (die_with_merges && next_commit.parents.nr > 0) {
		FILE *fp;
		int i;
		/* write parents to MERGE_HEAD */
		fp = fopen(MERGE_HEAD, "w");
		if (!fp)
			return error("Could not open %s for writing.",
								MERGE_HEAD);
		for (i = 0; i < next_commit.parents.nr; ++i)
			if (fprintf(fp, "%s\n",
				    next_commit.parents.items[i].string) < 0)
				return error("Could not write into %s.",
								MERGE_HEAD);
		fclose(fp);

		msgfile = MERGE_MSG;
	} else
		msgfile = COMMIT_EDITMSG;
	if (write_commit_summary_into(msgfile))
		return 1;
	if (write_commit_summary_into(MESSAGE_FILE))
		return 1;
	return 0;
}

static int saved_options(const char *var, const char *value, void *cb)
{
	if (!strcmp(var, "sequencer.advice")) {
		advice = git_config_bool(var, value);
		return 0;
	}
	if (!strcmp(var, "sequencer.allowdirty")) {
		allow_dirty = git_config_bool(var, value);
		return 0;
	}
	if (!strcmp(var, "sequencer.author.name"))
		return git_config_string(&next_commit.author.name, var, value);
	if (!strcmp(var, "sequencer.author.mail"))
		return git_config_string(&next_commit.author.mail, var, value);
	if (!strcmp(var, "sequencer.author.time"))
		return git_config_string(&next_commit.author.time, var, value);
	if (!strcmp(var, "sequencer.encoding"))
		return git_config_string(&next_commit.encoding, var, value);
	if (!strcmp(var, "sequencer.headname"))
		return git_config_string(&orig_headname, var, value);
	if (!strcmp(var, "sequencer.skiphead"))
		return git_config_string(&skiphead, var, value);
	if (!strcmp(var, "sequencer.head")) {
		const char *head;
		if (git_config_string(&head, var, value))
			return 1;
		return get_sha1(head, orig_head_sha1);
	}
	if (!strcmp(var, "sequencer.pausedat")) {
		const char *paused_at;
		if (git_config_string(&paused_at, var, value))
			return 1;
		return get_sha1(paused_at, paused_at_sha1);
	}
	if (!strcmp(var, "sequencer.availmarks")) {
		const char *tmp1;
		if (git_config_string(&tmp1, var, value))
			return 1;
		while (tmp1) {
			const char *tmp2 = strchrnul(tmp1, ' ');
			string_list_append(xstrndup(tmp1, tmp2-tmp1),
							&available_marks);
			if (tmp2[0])
				tmp1 = tmp2+1;
			else
				tmp1 = NULL;
		}
		return 0;
	}
	if (!prefixcmp(var, "sequencer.caller.")) {
		const char *cmp = var+17, **expect;
		int action = 0;
		if (!strcmp(cmp, "abort")) {
			expect = &caller_abort;
			action = ACTION_ABORT;
		} else if (!strcmp(cmp, "continue")) {
			expect = &caller_continue;
			action = ACTION_CONTINUE;
		} else if (!strcmp(cmp, "skip")) {
			expect = &caller_skip;
			action = ACTION_SKIP;
		} else
			return 1;
		if (git_config_string(&cmp, var, value))
			return 1;
		if (strcmp(cmp, *expect)) {
			caller_check_failed |= action;
			*expect = cmp;
		}
		return 0;
	}
	if (!strcmp(var, "sequencer.squashcount")) {
		squash_count = git_config_int(var, value);
		return 0;
	}
	if (!strcmp(var, "sequencer.verbosity"))
		return set_verbosity(git_config_int(var, value));
	if (!strcmp(var, "sequencer.why")) {
		why = git_config_int(var, value);
		return 0;
	}
	return 1;
}

static int get_saved_options(void)
{
	memset(&available_marks, 0, sizeof(struct string_list));
	if (git_config_from_file(saved_options, SAVE_FILE, NULL))
		return 1;
	marks_total = available_marks.nr;
	return 0;
}

static void add_comment_to_donefile(const char *comment)
{
	FILE *fp = fopen(DONE_FILE, "a");
	if (!fp)
		return;
	fprintf(fp, "# %s\n", comment);
	fclose(fp);
}


/**********************************************************************
 * Several die() and restore functions
 */

/* Restore refs updated by ref insn */
static void restore_refs(void)
{
	struct strbuf buf = STRBUF_INIT;
	struct string_list list;
	FILE *fp = fopen(DESTINY_FILE, "r");
	if (!fp) {
		warning("Could not open file '%s': %s",
					DESTINY_FILE, strerror(errno));
		fprintf(stderr, "Hence I cannot restore updated refs.\n");
		return;
	}
	memset(&list, 0, sizeof(struct string_list));
	while (strbuf_getline(&buf, fp, '\n') != EOF) {
		unsigned char sha[20];
		char *ref = strbuf_detach(&buf, NULL);

		if (strbuf_getline(&buf, fp, '\n') == EOF) {
			warning("This must not happen! "
				"Destiny file seems to be broken...");
			break;
		}

		if (!string_list_has_string(&list, ref)) {
			/* insert into list and restore ref */
			if (buf.len > 1) {
				if (get_sha1(buf.buf, sha)) {
					warning("Could not reset ref '%s' to "
					"%s, because the SHA1 does not exist.",
								ref, buf.buf);
					free(ref);
					continue;
				}
				update_ref(reflog, ref, sha, NULL, 0,
							MSG_ON_ERR);
			} else
				delete_ref(ref, NULL, 0);
			string_list_insert(ref, &list);
		} else
			free(ref);
	}
	strbuf_release(&buf);
	list.strdup_strings = 1; /* free items */
	string_list_clear(&list, 0);
}

static int restore(void)
{
	int failed;
	if (file_exists(DESTINY_FILE))
		restore_refs();
	failed = reset_almost_hard(orig_head_sha1);
	if (!failed && orig_headname)
		create_symref("HEAD", orig_headname, reflog);
	return failed;
}

static NORETURN void die_abort_cb(const char *err, va_list params)
{
	restore();
	cleanup();
	if (!params)
		fprintf(stderr, "%s", err);
	else
		vfprintf(stderr, err, params);
	fprintf(stderr, "\n");
	exit(1);
}

static void prepare_for_continue(void)
{
	if (batchmode)
		die_abort_cb("Aborting, because of batch mode.", NULL);
	save_int("sequencer.squashcount", squash_count);
	if (why == WHY_UNKNOWN)
		why = WHY_CONFLICT;
	save_int("sequencer.why", why);
	if (why == WHY_PAUSE || why == WHY_RUN) {
		if (get_sha1("HEAD", head_sha1))
			die("You do not have a valid HEAD.");
		save("sequencer.pausedat", sha1_to_hex(head_sha1));
	}
	if (why != WHY_TODO) {
		int i;
		struct strbuf tmp = STRBUF_INIT;
		save_commit_data();
		for (i = 0; i < available_marks.nr; ++i) {
			strbuf_addstr(&tmp, available_marks.items[i].string);
			strbuf_addch(&tmp, ' ');
		}
		save("sequencer.availmarks", tmp.buf);
		strbuf_release(&tmp);
	}
	print_advice();
	small_cleanup();
}

static NORETURN void die_continue_cb(const char *err, va_list params)
{
	vfprintf(stderr, err, params);
	fprintf(stderr, "\n");
	prepare_for_continue();
	exit(3);
}


/**********************************************************************
 * "patch" related helper functions
 */

/* Invoke git-apply, to be used by insn patch */
static int do_apply(const char *patch, const char *const *env, ...)
{
	va_list params;
	const char *args[50];
	const char *arg;
	int i = 0;
	int j;
	struct child_process chld;

	memset(&chld, 0, sizeof(chld));
	args[i++] = "apply";

	if (patch_opt.apply.nr >= ARRAY_SIZE(args))
		return error("too many arguments to git-apply.");
	for (j = 0; j < patch_opt.apply.nr; ++j)
		args[i++] = patch_opt.apply.items[j].string;

	va_start(params, env);
	while ((arg = va_arg(params, const char *)) &&
	       (i < ARRAY_SIZE(args) - 1))
		args[i++] = arg;
	va_end(params);
	args[i++] = NULL;
	if (arg)
		return error("too many arguments to git-apply.");

	chld.argv = args;
	chld.env = env;
	chld.git_cmd = 1;
	chld.in = -1;
	if (!verbosity || patch_opt.threeway) {
		/* When we are allowed to fall back to 3-way later,
		 * don't give false errors during the initial attempt. */
		chld.no_stdout = 1;
		chld.no_stderr = 1;
	}

	if (start_command(&chld))
		return 1;

	if (write(chld.in, patch, strlen(patch)) < 0) {
		close(chld.in);
		finish_command(&chld);
		return error("Could not write to git-apply.");
	}

	close(chld.in);
	return finish_command(&chld);
}

/* This is a slightly simplified version of write_cache_as_tree() for
 * temporary index files. */
static int write_cache_as_tree_temp(unsigned char *sha1, const char *indexfile)
{
	int entries, was_valid, newfd;
	struct lock_file *lock_file = xcalloc(1, sizeof(struct lock_file));

	newfd = hold_lock_file_for_update(lock_file, indexfile, 0);
	if (newfd < 0)
		return error("Could not lock %s.", indexfile);

	entries = read_cache_from(indexfile);
	if (entries < 0)
		return error("Could not read the temporary index.");

	if (!active_cache_tree)
		active_cache_tree = cache_tree();

	was_valid = cache_tree_fully_valid(active_cache_tree);

	if (!was_valid) {
		if (cache_tree_update(active_cache_tree,
				      active_cache, active_nr,
				      0, 0) < 0)
			return WRITE_TREE_UNMERGED_INDEX;
		if (0 <= newfd) {
			if (!write_cache(newfd, active_cache, active_nr) &&
			    !commit_lock_file(lock_file))
				newfd = -1;
		}
		/* Not being able to write is fine -- we are only interested
		 * in updating the cache-tree part, and if the next caller
		 * ends up using the old index with unupdated cache-tree part
		 * it misses the work we did here, but that is just a
		 * performance penalty and not a big deal.
		 */
	}

	hashcpy(sha1, active_cache_tree->sha1);

	if (0 <= newfd)
		rollback_lock_file(lock_file);

	return 0;
}

static int fallback_threeway(const char *patch, const char *subject)
{
	unsigned char base_sha1[20];
	unsigned char next_sha1[20];
	const char *env[2];
	char tmp[PATH_MAX];
	struct merge_options o;
	struct tree *result, *next_tree, *base_tree, *head_tree;
	static struct lock_file index_lock;
	int index_fd, clean;
	const char *index_file = git_path(SEQ_DIR "/index-3way");

	/* First see if the patch records the index info that we can use. */
	if (do_apply(patch, NULL, "--build-fake-ancestor", index_file, NULL))
		return error("Could not build fake ancestor for 3way merge.");

	discard_cache();
	if (write_cache_as_tree_temp(base_sha1, index_file)) {
		unlink(index_file);
		if (verbosity)
			fprintf(stderr, "Repository lacks necessary blobs "
					"to fall back on 3-way merge.\n");
		return 1;
	}

	if (verbosity)
		puts("Using index info to reconstruct a base tree...");

	memset(env, 0, sizeof(env));
	snprintf(tmp, PATH_MAX, "GIT_INDEX_FILE=%s", index_file);
	env[0] = tmp;

	if (do_apply(patch, env, "--cached", NULL)) {
		unlink(index_file);
		return error("Patch does not apply to blobs recorded in its index.");
	}

	discard_cache();
	if (write_cache_as_tree_temp(next_sha1, index_file)) {
		unlink(index_file);
		return 1;
	}
	unlink(index_file);

	if (verbosity)
		puts("Falling back to patching base and 3-way merge...");

	/*
	 * This is not so wrong.  Depending on which base we picked,
	 * the merge base (base_sha1) may be wildly different from ours,
	 * but the new tree (next_sha1) has the same set of wildly
	 * different changes in parts the patch did not touch, so
	 * recursive ends up canceling them, saying that we reverted
	 * all those changes.
	 */

	discard_cache();
	index_fd = hold_locked_index(&index_lock, 0);
	if (index_fd < 0)
		return error("Unable to create locked index: %s",
						strerror(errno));

	read_cache();
	init_merge_options(&o);
	o.branch1 = "HEAD";
	o.branch2 = subject;
	base_tree = parse_tree_indirect(base_sha1);
	head_tree = parse_tree_indirect(head_sha1);
	next_tree = parse_tree_indirect(next_sha1);

	clean = merge_trees(&o, head_tree, next_tree, base_tree, &result);
	if ((write_cache(index_fd, active_cache, active_nr) ||
	     commit_locked_index(&index_lock)))
		return error("Could not write new index file.");

	discard_cache();
	if (!clean) {
		rerere();
		write_commit_summary_into(MERGE_MSG);
		return error("Failed to merge in the changes.");
	}
	return 0;
}

/* A helper for mailinfo() */
static char *get_info_from_infofile(char *buf, const char *what)
{
	char *tmp1, *tmp2;
	char *info = NULL;

	tmp1 = strstr(buf, what);
	if (tmp1) {
		char c;
		tmp1 += strlen(what);
		tmp2 = strpbrk(tmp1, "\r\n");
		if (tmp2) {
			c = *tmp2;
			*tmp2 = 0;
		}
		info = xstrdup(tmp1);
		if (tmp2)
			*tmp2 = c;
	}
	return info;
}

/*
 * Invoke git-mailinfo, to be used by insn patch.
 * Set info, if return value is 0, otherwise info is in undefined state.
 */
static int mailinfo(int ifd, struct commit_info *info)
{
	const char *args[5];
	int i = 0;
	struct child_process chld;
	struct strbuf buf = STRBUF_INIT;
	char *msgfile = git_pathdup(SEQ_DIR "/patch-msg");
	char time[50];

	memset(&chld, 0, sizeof(chld));

	args[i++] = "mailinfo";
	if (patch_opt.keep)
		args[i++] = "-k";
	if (patch_opt.noutf8)
		args[i++] = "-n";
	args[i++] = msgfile;
	args[i++] = PATCH_FILE;
	args[i++] = NULL;

	chld.argv = args;
	chld.git_cmd = 1;
	chld.in = ifd;
	chld.out = -1;

	if (start_command(&chld))
		return 1;

	if (strbuf_read(&buf, chld.out, 1024) < 0) {
		close(chld.out);
		finish_command(&chld);
		return error("Could not read from git-mailinfo.");
	}

	close(chld.out);
	if (finish_command(&chld)) {
		strbuf_release(&buf);
		return 1;
	}

	/* parse the information from the infofile */
	info->subject = get_info_from_infofile(buf.buf, "Subject: ");
	info->author.name = get_info_from_infofile(buf.buf, "Author: ");
	info->author.mail = get_info_from_infofile(buf.buf, "Email: ");
	info->author.time = get_info_from_infofile(buf.buf, "Date: ");
	if (info->author.time) {
		parse_date(info->author.time, time, sizeof(time));
		free((void *)info->author.time);
		info->author.time = xstrdup(time);
	}
	strbuf_release(&buf);

	/* get message */
	if (info->subject) {
		next_commit.subject = xstrdup(info->subject);
		strbuf_addstr(&buf, info->subject);
		strbuf_addstr(&buf, "\n\n");
	}

	if (run_hook(NULL, "applypatch-msg", msgfile, NULL))
		return error("Hook applypatch-msg failed.");
	if (strbuf_read_file(&buf, msgfile, 1024) < 0)
		return error("Could not read message file %s: %s",
				msgfile, strerror(errno));
	stripspace(&buf, 0);

	unlink(msgfile);
	free(msgfile);

	strbuf_addbuf(&info->summary, &buf);
	strbuf_release(&buf);

	/* get patch */
	if (file_exists(PATCH_FILE)) {
		if (strbuf_read_file(&buf, PATCH_FILE, 4096) < 0)
			return error("Could not read patch %s: %s",
					PATCH_FILE, strerror(errno));
		info->patch = strbuf_detach(&buf, NULL);
	}
	return 0;
}


/**********************************************************************
 * "squash" related helper functions
 */

static char *nth_string(int i)
{
	static char nth[50];
	const char *suffix;
	switch (i % 10) {
	case 1:
		suffix = "st";
		break;
	case 2:
		suffix = "nd";
		break;
	case 3:
		suffix = "rd";
		break;
	default:
		suffix = "th";
	}
	if ((i / 10) % 10 == 1) /* 11th, 12th, 13th */
		suffix = "th";
	snprintf(nth, sizeof(nth), "%d%s", i, suffix);
	return nth;
}

static int make_squash_message_multiple(unsigned char *sha,
					unsigned char *base_sha)
{
	struct strbuf buf;
	char range[50];
	struct rev_info revs;
	struct commit *commit;
	int i = squash_count;
	init_revisions(&revs, NULL);
	snprintf(range, sizeof(range), "%s..HEAD", sha1_to_hex(sha));
	revs.abbrev = 40;
	revs.reverse = 1;
	if (handle_revision_arg(range, &revs, 0, 1))
		return error("Revision argument handling failed.");
	if (prepare_revision_walk(&revs))
		return error("Revision walk setup failed.");
	strbuf_init(&buf, 8192);
	while ((commit = get_revision(&revs))) {
		/* write base_sha to return it */
		if (i == squash_count && base_sha)
			hashcpy(base_sha, commit->object.sha1);
		/* write commit messages into buf */
		strbuf_addf(&buf, "\n# This is the %s commit message:",
							nth_string(++i));
		strbuf_addstr(&buf, strstr(commit->buffer, "\n\n"));
	}
	squash_count = i;
	/* prepend an introduction message */
	strbuf_addf(&next_commit.summary,
		    "# This is a combination of %d commits.\n%s",
						squash_count, buf.buf);
	strbuf_release(&buf);
	return 0;
}

static void make_squash_message(struct strbuf *msg)
{
	char *tmp;
	if (squash_count)
		++squash_count;
	else
		squash_count = 2;

	strbuf_addf(&next_commit.summary,
		    "# This is a combination of %d commits.\n", squash_count);

	if (file_exists(SQUASH_MSG)) {
		struct strbuf buf;
		strbuf_init(&buf, 8192);
		if (strbuf_read_file(&buf, SQUASH_MSG, 0) < 0) {
			warning("Could not read message file %s.",
							SQUASH_MSG);
			goto rest;
		}
		if (!prefixcmp(buf.buf, "# This is a")) {
			tmp = strchr(buf.buf, '\n');
			if (!tmp)
				tmp = buf.buf;
			strbuf_addf(&next_commit.summary, "%s\n", tmp+1);
		}
		strbuf_release(&buf);
	} else {
		struct commit *head = lookup_commit_reference(head_sha1);
		if (!head)
			goto rest;
		tmp = strstr(head->buffer, "\n\n");
		if (tmp)
			strbuf_addf(&next_commit.summary,
				    "# This is the 1st commit message:%s\n",
									tmp);
	}
rest:
	strbuf_addf(&next_commit.summary,
			"# This is the %s commit message:\n\n",
						nth_string(squash_count));
	strbuf_addbuf(&next_commit.summary, msg);
}

static int peek_next_insn(void)
{
	struct parsed_insn *cur = contents.cur->next;
	for (; cur && !cur->argv; cur = cur->next);
	if (!cur)
		return -1;
	return find_insn(cur->argv[0]);
}


/**********************************************************************
 * Helpers for the sanity check phase
 */

/* Raise an error on todo checking */
static int todo_error(const char *fmt, ...)
{
	va_list params;
	char msg[256];

	snprintf(msg, sizeof(msg), "error at line %d: %s\n", todo_line, fmt);
	va_start(params, fmt);
	vfprintf(stderr, msg, params);
	va_end(params);

	return 1;
}

/* Print a warning on todo checking */
static void todo_warn(const char *fmt, ...)
{
	va_list params;
	char msg[256];

	snprintf(msg, sizeof(msg), "warning at line %d: %s\n", todo_line, fmt);
	va_start(params, fmt);
	vfprintf(stderr, msg, params);
	va_end(params);
}

/* Return 1 if arg is a mark (must be colon-prefixed), 0 otherwise */
static int arg_is_mark(const char *arg)
{
	if (!unsorted_string_list_has_string(&available_marks, arg))
		return !todo_error("Mark %s is not yet defined.", arg);
	return 1;
}

/* Return commit struct if arg is a commit, NULL + todo_error otherwise */
static struct commit *arg_is_commit(const char *arg)
{
	unsigned char sha1[20];
	struct commit *commit;
	if (get_sha1(arg, sha1)) {
		todo_error("Could not find '%s'.", arg);
		return NULL;
	}
	if (!(commit = lookup_commit_reference_gently(sha1, 1))) {
		todo_error("Object '%s' is no commit.", arg);
		return NULL;
	}
	return commit;
}

/* Check if arg is in available_marks or a real commit */
static int arg_is_mark_or_commit(const char *arg)
{
	if (arg[0] == ':')
		return arg_is_mark(arg);
	else
		return (arg_is_commit(arg) != NULL);
}


/**********************************************************************
 * Functions for dealing with general options
 */

static int check_general_options(void)
{
	int msg = 0;
	if (opt.author) {
		const char *tmp1;
		tmp1 = strstr(opt.author, " <");
		if (tmp1 < opt.author+1 || tmp1[strlen(tmp1)-1] != '>')
			return todo_error("Author '%s' not in the correct format 'Name <e-mail>'.",
							opt.author);
	}
	if (opt.reuse_message) {
		++msg;
		if (!arg_is_commit(opt.reuse_message))
			return 1;
	}
	if (opt.message)
		++msg;
	if (opt.file) {
		++msg;
		if (!file_exists(opt.file))
			return todo_error("File '%s' does not exist.", opt.file);
		/* Also check for readability? */
	}
	if (opt.edit && batchmode)
		return todo_error("--batch and --edit options do not make sense together");
	if (opt.reuse_commit) {
		if (!arg_is_commit(opt.reuse_commit))
			return 1;
		if (msg)
			todo_warn("-M/-m/-F override message of -C option.");
	}
	return 0;
}

static int has_general_options(void)
{
	return (opt.reuse_commit || opt.author || opt.file || opt.message ||
		opt.reuse_message || opt.signoff || opt.edit);
}

static int handle_general_options(void)
{
	if (opt.reuse_commit) {
		struct commit *commit = get_commit(opt.reuse_commit);
		strbuf_reset(&next_commit.summary);
		if (next_commit.author.name)
			free((void *)next_commit.author.name);
		if (next_commit.author.mail)
			free((void *)next_commit.author.mail);
		if (next_commit.author.time)
			free((void *)next_commit.author.time);
		if (next_commit.encoding)
			free((void *)next_commit.encoding);
		get_commit_info(commit, 0);
	}
	if (opt.author) {
		if (next_commit.author.name)
			free((void *)next_commit.author.name);
		if (next_commit.author.mail)
			free((void *)next_commit.author.mail);
		if (next_commit.author.time) {
			free((void *)next_commit.author.time);
			next_commit.author.time = NULL; /* reset time */
		}
		set_author_info(opt.author);
	}
	if (opt.reuse_message) {
		struct commit *commit = get_commit(opt.reuse_message);
		const char *tmp = strstr(commit->buffer, "\n\n");
		strbuf_reset(&next_commit.summary);
		if (tmp)
			strbuf_addstr(&next_commit.summary, tmp+2);
		set_message_source("message");
	}
	if (opt.file) {
		strbuf_reset(&next_commit.summary);
		if (strbuf_read_file(&next_commit.summary, opt.file, 8192) < 0)
			return error("Could not read message file %s.",
								opt.file);
		set_message_source("message");
	}
	if (opt.message) {
		strbuf_reset(&next_commit.summary);
		strbuf_addstr(&next_commit.summary, opt.message);
		set_message_source("message");
	}
	/* add trailing \n to message */
	if (next_commit.summary.len &&
	    next_commit.summary.buf[next_commit.summary.len-1] != '\n')
		strbuf_addch(&next_commit.summary, '\n');
	if (opt.signoff) {
		struct strbuf tmp = STRBUF_INIT;
		int i;

		strbuf_addstr(&tmp, "Signed-off-by: ");
		strbuf_addstr(&tmp, fmt_name(getenv("GIT_COMMITTER_NAME"),
					     getenv("GIT_COMMITTER_EMAIL")));
		strbuf_addch(&tmp, '\n');
		for (i = next_commit.summary.len-1; i > 0 &&
				next_commit.summary.buf[i-1] != '\n'; i--);
		if (prefixcmp(next_commit.summary.buf + i, tmp.buf)) {
			if (prefixcmp(next_commit.summary.buf + i,
							"Signed-off-by: "))
				strbuf_addch(&next_commit.summary, '\n');
			strbuf_addbuf(&next_commit.summary, &tmp);
		}
		strbuf_release(&tmp);
	}
	return 0;
}

static int has_general_message_option(void)
{
	return opt.reuse_commit || opt.reuse_message || opt.file || opt.message;
}



/**********************************************************************
 * Data structures and functions for TODO instructions
 */

static struct option no_insn_options[] = {
	OPT_END(),
};


/* noop */
static const char * const insn_noop_usage[] = {
	"noop",
	NULL
};

static int insn_noop_check(int argc, const char **argv)
{
	if (argc)
		return todo_error("Instruction takes no arguments.");
	return 0;
}

static int insn_noop_act(int argc, const char **argv)
{
	return 0;
}


/* pause */
static const char * const insn_pause_usage[] = {
	"pause",
	NULL
};

static int insn_pause_check(int argc, const char **argv)
{
	if (batchmode)
		return todo_error("Instruction does not make sense in batch mode.");
	if (argc)
		return todo_error("Instruction takes no arguments.");
	return 0;
}

static int insn_pause_act(int argc, const char **argv)
{
	/* We do not use reset_next_commit() after "pick" in the "edit"
	 * insn, so perhaps next_commit is still set. We take a non-empty
	 * commit message as evidence that next_commit is still set. */
	if (message_is_empty()) {
		struct commit *commit = lookup_commit_reference(head_sha1);
		get_commit_info(commit, 1);
	}
	why = WHY_PAUSE;
	prepare_for_continue();
	exit(2);
}


/* pick */
static const char * const insn_pick_usage[] = {
	"pick [options] <commit>",
	NULL
};

static struct option insn_pick_options[] = {
	OPT_BOOLEAN('R', "reverse", &pick_opt.reverse,
		"revert introduced changes"),
	OPT_INTEGER(0, "mainline", &pick_opt.mainline,
		"specify parent number to use for merge commits"),
	OPT_GENERAL_OPTIONS,
	OPT_END(),
};

static void insn_pick_init(void)
{
	memset(&pick_opt, 0, sizeof(pick_opt));
}

static int insn_pick_check(int argc, const char **argv)
{
	struct commit *commit;
	if (argc != 1)
		return todo_error("Wrong number of arguments. "
				  "(%d given, 1 wanted)", argc);
	if (!(commit = arg_is_commit(argv[0])))
		return 1;
	if (check_general_options())
		return 1;
	if (pick_opt.mainline) {
		int parents = commit_list_count(commit->parents);
		if (parents < pick_opt.mainline)
			return todo_error("Commit has only %d (less than %d) "
					"parents.", parents, pick_opt.mainline);
		if (parents <= 1)
			todo_warn("Commit is not a merge at all.");
	}
	return 0;
}

static int insn_pick_act(int argc, const char **argv)
{
	struct commit *commit = get_commit(argv[0]);
	const char *author;
	int failed;
	int pick_flags = 0;

	set_pick_subject(argv[0], commit);

	if (pick_opt.reverse)
		pick_flags |= PICK_REVERSE;

	/* Be kind to users and ignore --mainline=1 on non-merge commits */
	if (pick_opt.mainline && commit_list_count(commit->parents) < 2)
		pick_opt.mainline = 0;

	if (!pick_opt.reverse && fast_forward_possible(commit) &&
	    !has_general_options())
		return do_fast_forward(commit->object.sha1);

	failed = pick_commit(commit, pick_opt.mainline, pick_flags,
						&next_commit.summary);
	set_message_source(sha1_to_hex(((struct object *)commit)->sha1));
	author = strstr(commit->buffer, "\nauthor ");
	if (author)
		set_author_info(author + 8);

	/* We do not want extra Conflicts: lines on cherry-pick,
	   so just take the old commit message. */
	if (failed && !pick_opt.reverse) {
		strbuf_setlen(&next_commit.summary, 0);
		strbuf_addstr(&next_commit.summary,
				strstr(commit->buffer, "\n\n")+2);
	}

	if (handle_general_options())
		return 1;

	if (!failed && (message_is_empty() || opt.edit))
		if (edit_message(1))
			return 1;

	if (failed) {
		rerere();
		make_patch(commit);
		write_commit_summary_into(MERGE_MSG);
		return error("Picking %s failed.",
				sha1_to_hex(commit->object.sha1));
	}

	if (do_commit(head_sha1))
		return error("Could not commit.");

	return 0;
}


/* patch */
static const char * const insn_patch_usage[] = {
	"patch [options] <file>",
	NULL
};

static int apply_opt_cb(const struct option *opt, const char *arg, int unset)
{
	struct strbuf buf = STRBUF_INIT;
	if (unset)
		return 0;
	if (arg) {
		if (opt->long_name) {
			if (!strcmp("context", opt->long_name))
				strbuf_addf(&buf, "-C%s", arg);
			else
				strbuf_addf(&buf, "--%s=%s",
					    opt->long_name, arg);
		} else
			strbuf_addf(&buf, "-%c%s", opt->short_name, arg);
	} else {
		if (opt->long_name)
			strbuf_addf(&buf, "--%s", opt->long_name);
		else
			strbuf_addf(&buf, "-%c", opt->short_name);
	}
	string_list_append(buf.buf, &patch_opt.apply);
	strbuf_release(&buf);
	return 0;
}

static struct option insn_patch_options[] = {
	OPT_BOOLEAN('3', "3way", &patch_opt.threeway,
		"fall back to 3-way merge"),
	OPT_BOOLEAN('k', NULL, &patch_opt.keep,
		"pass to git-mailinfo (keep subject)"),
	OPT_BOOLEAN('n', NULL, &patch_opt.noutf8,
		"pass to git-mailinfo (no utf8)"),
	OPT_GENERAL_OPTIONS,
	OPT_GROUP("Options passed to git-apply"),
	{ OPTION_CALLBACK, 'R', "reverse", NULL, NULL,
	  "reverse changes", PARSE_OPT_NOARG, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "context", NULL, "n",
	  "ensure context of <n> lines", 0, apply_opt_cb },
	{ OPTION_CALLBACK, 'p', NULL, NULL, "n",
	  "remove <n> leading slashes", 0, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "unidiff-zero", NULL, NULL,
	  "bypass unidiff checks", PARSE_OPT_NOARG, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "exclude", NULL, "path-pattern",
	  "do not apply changes to given files", 0, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "no-add", NULL, NULL,
	  "ignore additions of patch", PARSE_OPT_NOARG, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "whitespace", NULL, "action",
	  "set whitespace error behavior", 0, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "inaccurate-eof", NULL, NULL,
	  "support inaccurate EOFs", PARSE_OPT_NOARG, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "recount", NULL, NULL,
	  "ignore line counts in hunk headers", PARSE_OPT_NOARG, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "reject", NULL, NULL,
	  "save rejected hunks in .rej files", PARSE_OPT_NOARG, apply_opt_cb },
	{ OPTION_CALLBACK, 0, "directory", NULL, "root",
	  "prepend <root> to all filenames", 0, apply_opt_cb },
	OPT_END(),
};

static void insn_patch_init(void)
{
	memset(&patch_opt, 0, sizeof(patch_opt));
	patch_opt.apply.strdup_strings = 1;
}

static int insn_patch_check(int argc, const char **argv)
{
	struct strbuf buf = STRBUF_INIT;
	if (argc != 1)
		return todo_error("Wrong number of arguments. "
				  "(%d given, 1 wanted)", argc);
	if (check_general_options())
		return 1;
	if (strbuf_read_file(&buf, argv[0], 8192) < 0)
		return todo_error("Cannot read patch file %s: %s",
						argv[0], strerror(errno));
	if (prefixcmp(buf.buf, "diff") && !strstr(buf.buf, "\ndiff")) {
		strbuf_release(&buf);
		return todo_error("Patch file '%s' contains no patch.",
								argv[0]);
	}
	strbuf_release(&buf);
	return 0;
}

static int insn_patch_act(int argc, const char **argv)
{
	int ifd;
	struct commit_info info;

	ifd = open(argv[0], O_RDONLY);
	if (ifd < 0)
		return error("Could not open patch %s.", argv[0]);

	memset(&info, 0, sizeof(info));
	strbuf_init(&info.summary, 0);

	if (mailinfo(ifd, &info))
		return error("Could not read or parse mail.");
	/* ifd is closed now */

	/* Ignore every mail that's not containing a patch */
	if (!info.patch) {
		fprintf(stderr, "Does not contain patch!");
		return 0;
	}

	strbuf_addbuf(&next_commit.summary, &info.summary);
	set_message_source("message");

	if (info.author.name)
		next_commit.author.name = xstrdup(info.author.name);
	if (info.author.mail)
		next_commit.author.mail = xstrdup(info.author.mail);
	if (info.author.time)
		next_commit.author.time = xstrdup(info.author.time);

	if (handle_general_options())
		return 1;

	if (do_apply(info.patch, NULL, "--index", NULL)) {
		if (patch_opt.threeway &&
		    !fallback_threeway(info.patch, info.subject)) {
			/*
			 * Applying the patch to an earlier tree and merging
			 * the result may have produced the same tree as ours.
			 */
			free_commit_info(&info);
			if (read_cache() < 0)
				return error("Could not read the index.");
			if (!index_differs_from("HEAD",
						DIFF_OPT_IGNORE_SUBMODULES)) {
				if (verbosity > 1)
					puts("No changes -- Patch already applied.");
				return 0; /* don't commit */
				/*
				 * All those author/message options to "patch"
				 * are for nothing now, but do we really have
				 * a choice here?
				 */
			}
			/* successfully merged */
		} else {
			free_commit_info(&info);
			return error("Applying patch failed.");
		}
	}
	free_commit_info(&info);

	if (run_hook(NULL, "pre-applypatch", NULL))
		return error("Hook pre-applypatch failed.");

	if (message_is_empty() || opt.edit) {
		strbuf_addstr(&next_commit.summary, "\n"
"# Please enter the commit message for the applied patch.\n"
"# (Comment lines starting with '#' will not be included)\n");
		if (edit_message(1))
			return 1;
	}

	discard_cache();

	if (do_commit(head_sha1))
		return error("Could not commit.");

	/* We ignore errors of the post-applypatch hook. */
	run_hook(NULL, "post-applypatch", NULL);
	return 0;
}


/* edit */
static const char * const insn_edit_usage[] = {
	"edit <commit>",
	NULL
};

static int insn_edit_check(int argc, const char **argv)
{
	if (batchmode)
		return todo_error("'edit' instruction and --batch do not "
						"make sense together.");
	return insn_pick_check(argc, argv);
}

static int insn_edit_act(int argc, const char **argv)
{
	if (insn_pick_act(argc, argv))
		return 1;

	add_comment_to_donefile("pausing");

	if (get_sha1("HEAD", head_sha1))
		return error("You do not have a valid HEAD.");

	if (insn_pause_act(0, NULL))
		return 1;
	return 0;
}


/* squash */
static const char * const insn_squash_usage[] = {
	"squash <commit>",
	"squash [options] --from <mark>",
	NULL
};

static struct option insn_squash_options[] = {
	OPT_BIT(0, "from", &squash_opt,
		"squash all commits from <mark>", OPT_SQUASH_FROM),
	OPT_BIT(0, "include-merges", &squash_opt,
		"do not fail on merge commits", OPT_SQUASH_INCLUDE_MERGES),
	OPT_GENERAL_OPTIONS,
	OPT_END(),
};

static void insn_squash_init(void)
{
	squash_opt = 0;
}

static int insn_squash_check(int argc, const char **argv)
{
	if (argc != 1)
		return todo_error("Wrong number of arguments. "
				  "(%d given, 1 wanted)", argc);
	if (squash_opt & OPT_SQUASH_FROM) {
		if (argv[0][0] != ':')
			return todo_error("squash --from <mark> needs a "
						"colon-prefixed mark.");
		if (!(squash_opt & OPT_SQUASH_INCLUDE_MERGES)) {
			int i = 0;
			/* find mark in available_marks */
			for (i = 0; i < available_marks.nr &&
			     strcmp(available_marks.items[i].string, argv[0]);
			     ++i);
			if (i == available_marks.nr)
				return todo_error("Mark %s not found.", argv[0]);

			/* Let's look for forbidden reset or merges.
			 * Those are marked with "m" or "r" */
			for (; i < available_marks.nr; ++i) {
				if (available_marks.items[i].string[0] == 'm')
					return todo_error("There is a merge "
					 "instruction below mark %s. You may "
					     " try --include-merges", argv[0]);
				if (available_marks.items[i].string[0] == 'r')
					return todo_error("There is a reset "
					 "instruction below mark %s. You may "
					     "try --include-merges", argv[0]);
			}
		}
		return !arg_is_mark(argv[0]);
	} else
		return !arg_is_commit(argv[0]);
	if (check_general_options())
		return 1;
	return 0;
}

static int insn_squash_act(int argc, const char **argv)
{
	unsigned char base_sha1[20];
	unsigned char parent_sha1[20];
	unsigned char *parent = parent_sha1;
	struct commit *commit;
	int failed = 0;
	int multisquash = (peek_next_insn() == find_insn("squash"));

	/*
	 * Hm, somehow I don't think --skip on a conflicting squash
	 * may be useful, but if someone wants to do it, it should
	 * do the obvious: skip what squash would do.
	 */
	save("sequencer.skiphead", sha1_to_hex(head_sha1));

	if (squash_opt & OPT_SQUASH_FROM) {
		char ref[50];
		mark_to_ref(argv[0], ref, 1);
		commit = get_commit(ref);
		hashcpy(parent_sha1, commit->object.sha1);
		make_squash_message_multiple(parent_sha1, base_sha1);
		failed = update_ref(reflog, "HEAD", base_sha1, head_sha1, 0,
				    MSG_ON_ERR);
	} else {
		struct strbuf pick_msg = STRBUF_INIT;
		commit = get_commit(argv[0]);
		set_pick_subject(argv[0], commit);
		failed = pick_commit(commit, 0, 0, &pick_msg);
		if (failed)
			make_patch(commit);
		make_squash_message(&pick_msg);
		strbuf_release(&pick_msg);
		hashcpy(base_sha1, head_sha1);
		parent = NULL; /* we rely on next_commit.parents */
		if (get_sha1("HEAD^", parent_sha1))
			hashcpy(parent_sha1, null_sha1);
	}
	commit = lookup_commit_reference(base_sha1);
	get_commit_header(commit, 1);

	set_message_source("squash");
	if (handle_general_options())
		return 1;

	if (!failed && !multisquash &&
	    (!has_general_message_option() || opt.edit))
		if (edit_message(1))
			return 1;

	if (failed) {
		if (squash_opt & OPT_SQUASH_FROM)
			/* Hm, this means the update_ref() has failed. */
			return error("Squashing failed.");
		else {
			rerere();
			write_commit_summary_into(MERGE_MSG);
			update_ref(reflog, "HEAD", parent_sha1, head_sha1,
								0, MSG_ON_ERR);
			return error("Squashing %s failed.", argv[0]);
		}
	}

	if (multisquash) /* do not commit */
		return write_commit_summary_into(SQUASH_MSG);

	if (do_commit(parent))
		return error("Could not commit.");

	return 0;
}


/* mark */
static const char * const insn_mark_usage[] = {
	"mark <mark>",
	NULL
};

static int insn_mark_check(int argc, const char **argv)
{
	int i = 0;
	int has_colon = 0;
	char *mark;
	if (argc != 1)
		return todo_error("Wrong number of arguments. "
				  "(%d given, 1 wanted)", argc);
	if (argv[0][0] == ':') {
		i++;
		has_colon = 1;
	}
	/* on "mark", the leading ":" may be omitted */
	for (; argv[0][i]; ++i)
		if (argv[0][i] < '0' || argv[0][i] > '9')
			return todo_error("Mark '%s' is not an integer.",
					has_colon ? argv[0]+1 : argv[0]);

	if (has_colon)
		mark = xstrdup(argv[0]);
	else {
		mark = xmalloc(strlen(argv[0]) + 2);
		sprintf(mark, ":%s", argv[0]);
	}
	if (string_list_has_string(&available_marks, mark))
		return todo_error("Mark %s already defined. "
				"Choose another integer.", mark);
	string_list_append(mark, &available_marks);
	return 0;
}

static int insn_mark_act(int argc, const char **argv)
{
	char ref[50];
	if (mark_to_ref(argv[0], ref, 1))
		return 1;
	++available_marks.nr;
	return update_ref(reflog, ref, head_sha1, NULL, 0, MSG_ON_ERR);
}


/* merge */
static const char * const insn_merge_usage[] = {
	"merge [options] <commit-ish> ...",
	NULL
};

static struct option insn_merge_options[] = {
	OPT_BOOLEAN(0, "standard", &merge_opt.standard,
		"generate default commit message"),
	OPT_STRING('s', "strategy", &merge_opt.strategy, "strategy",
		"Specify merge strategy"),
	OPT_GENERAL_OPTIONS,
	OPT_END(),
};

static void insn_merge_init(void)
{
	memset(&merge_opt, 0, sizeof(merge_opt));
}

static int insn_merge_check(int argc, const char **argv)
{
	int i;
	if (!argc)
		return todo_error("What are my parents? Need new parents!");

	for (i = 0; i < argc; ++i)
		if (!arg_is_mark_or_commit(argv[i]))
			return 1;
	if (check_general_options())
		return 1;

	string_list_append(xstrdup("m"), &available_marks);
	return 0;
}

static int insn_merge_act(int argc, const char **argv)
{
	int i;
	for (i = 0; i < argc; ++i) {
		unsigned char sha[20];
		mark_to_sha1(argv[i], sha);
		string_list_append(xstrdup(sha1_to_hex(sha)),
						&next_commit.parents);
	}

	set_message_source("merge");
	if (handle_general_options())
		return 1;

	if (merge_opt.standard) {
		struct strbuf msg = STRBUF_INIT;

		/* if there is some summary, ensure \n\n at the end */
		if (next_commit.summary.len > 1) {
			if (next_commit.summary.buf[next_commit.summary.len - 2] != '\n')
				strbuf_addch(&next_commit.summary, '\n');
			if (next_commit.summary.buf[next_commit.summary.len - 2] != '\n')
				strbuf_addch(&next_commit.summary, '\n');
		} else if (next_commit.summary.len == 1) {
			if (next_commit.summary.buf[0] == '\n')
				strbuf_release(&next_commit.summary);
			else
				strbuf_addstr(&next_commit.summary, "\n\n");
		}

		for (i = 0; i < argc; i++) {
			if (argv[i][0] == ':')
				merge_name(next_commit.parents.items[i].string,
									&msg);
			else
				merge_name(argv[i], &msg);
		}
		fmt_merge_msg(0, &msg, &next_commit.summary);
		strbuf_release(&msg);
	} else /* try fast-forward on merge -C .. ... */
	if (!opt.author && !opt.file && !opt.message && !opt.reuse_message &&
	    !opt.signoff && !opt.edit && opt.reuse_commit) {
		struct commit *commit = get_commit(opt.reuse_commit);
		if (fast_forward_possible(commit))
			return do_fast_forward(commit->object.sha1);
	}

	if (message_is_empty() || opt.edit) {
		strbuf_addstr(&next_commit.summary, "\n"
"# Please enter the merge commit message.\n"
"# (Comment lines starting with '#' will not be included)\n");
		if (edit_message(1))
			return 1;
	}

	if (do_merge()) {
		rerere();
		if (opt.reuse_commit)
			make_patch(get_commit(opt.reuse_commit));
		die_with_merges = 1;
		return error("Could not merge.");
	}
	if (do_commit(head_sha1))
		return error("Could not commit.");
	return 0;
}


/* reset */
static const char * const insn_reset_usage[] = {
	"reset <commit-ish>",
	NULL
};

static int insn_reset_check(int argc, const char **argv)
{
	if (argc != 1)
		return todo_error("Wrong number of arguments. "
				  "(%d given, 1 wanted)", argc);
	string_list_append(xstrdup("r"), &available_marks);
	return !arg_is_mark_or_commit(argv[0]);
}

static int insn_reset_act(int argc, const char **argv)
{
	unsigned char sha1[20];

	if (mark_to_sha1(argv[0], sha1))
		return error("Could not get SHA1 for '%s'.", argv[0]);
	reset_almost_hard(sha1);
	++available_marks.nr;
	return 0;
}


/* ref */
static const char * const insn_ref_usage[] = {
	"ref <ref>",
	NULL
};

static int insn_ref_check(int argc, const char **argv)
{
	if (argc != 1)
		return todo_error("Wrong number of arguments. "
				  "(%d given, 1 wanted)", argc);
	return 0;
}

static int insn_ref_act(int argc, const char **argv)
{
	unsigned char sha[20], *old_sha1 = NULL;
	FILE *fp = fopen(DESTINY_FILE, "a");
	if (!fp)
		return error("Could not open file '%s': %s",
					DESTINY_FILE, strerror(errno));
	if (get_sha1(argv[0], sha))
		fprintf(fp, "%s\n-\n", argv[0]);
	else {
		old_sha1 = sha;
		fprintf(fp, "%s\n%s\n", argv[0], sha1_to_hex(sha));
	}
	fclose(fp);
	return update_ref(reflog, argv[0], head_sha1, old_sha1, 0, MSG_ON_ERR);
}


/* run */
static const char * const insn_run_usage[] = {
	"run [--dir=<path>] [--] <cmd> <args>...",
	NULL
};

static void insn_run_init(void)
{
	memset(&run_opt, 0, sizeof(run_opt));
}

static struct option insn_run_options[] = {
	OPT_STRING(0, "directory", &run_opt.dir, "path",
		"change directory before running command"),
	OPT_END(),
};

static int insn_run_check(int argc, const char **argv)
{
	if (argc < 1)
		return todo_error("No command given.");
	if (run_opt.dir) {
#ifndef __MINGW32__
		struct stat st;
		if (lstat(run_opt.dir, &st))
			return todo_error("Directory '%s' does not exist",
								run_opt.dir);
		if (!S_ISDIR(st.st_mode))
			return todo_error("Path '%s' is not a directory.",
								run_opt.dir);
#else
		return todo_error("Sorry, --directory is not implemented "
				  "for MinGW.");
#endif
	}
	return 0;
}

static NORETURN void fork_die_cb(const char *err, va_list params)
{
	fprintf(stderr, "fork error: ");
	vfprintf(stderr, err, params);
	fprintf(stderr, "\n");
	exit(128);
}

static int insn_run_act(int argc, const char **argv)
{
	int status;
	/* If forking fails (e.g. the command does not exist), die() is called.
	 * So we need to set a sane die routine for the fork. */
	set_die_routine(fork_die_cb);
	status = run_command_v_opt_cd_env(argv, RUN_COMMAND_NO_STDIN,
						run_opt.dir, NULL);
	set_die_routine(die_continue_cb);
	if (status) {
		struct commit *commit = lookup_commit_reference(head_sha1);
		get_commit_info(commit, 1);
		next_commit.subject = xstrdup(contents.cur->orig.buf);
		why = WHY_RUN;
		if (verbosity)
			printf("Program exited with code %d.", status);
		return 1;
	}
	discard_cache();

	return 0;
}


static struct instruction avail_insns[] = {
	INSTRUCTION_NO_OPTS(edit),
	INSTRUCTION_NO_OPTS(mark),
	INSTRUCTION(merge),
	INSTRUCTION_NO_OPTS(noop),
	INSTRUCTION(patch),
	INSTRUCTION_NO_OPTS(pause),
	INSTRUCTION(pick),
	INSTRUCTION_NO_OPTS(ref),
	INSTRUCTION_NO_OPTS(reset),
	INSTRUCTION(run),
	INSTRUCTION(squash),
	{ NULL, NULL, NULL, NULL, NULL }
};

/* Return the index in array "avail_insns" of the insn "name" */
static int find_insn(const char *name)
{
	int i;
	if (!name)
		return -1;
	/* expand rebase -i shortcuts: */
	if (name[0] && !name[1]) {
		switch (*name) {
			case 'e': name = "edit"; break;
			case 'p': name = "pick"; break;
			case 's': name = "squash"; break;
		}
	}
	for (i = 0; avail_insns[i].usage; ++i)
		if (!prefixcmp(*avail_insns[i].usage, name))
			return i;
	return -1;
}


/**********************************************************************
 * Functions for TODO parser
 */

/* Parse a line */
static int parse_line(char *buf, size_t len, int lineno,
		      struct parsed_insn **line)
{
	static int alloc = 0;
	static struct strbuf arg_sb = STRBUF_INIT;
	static enum {
		ST_START,
		ST_DELIMITER,
		ST_ARGUMENT,
		ST_ESCAPE,
		ST_DOUBLE_QUOTES,
		ST_DOUBLE_QUOTES_ESCAPE,
		ST_SINGLE_QUOTES,
	} state = ST_START;
	/* The current rules are as follows:
	 *  1. whitespace at the beginning is ignored
	 *  2. insn is everything up to next whitespace or EOL
	 *  3. now whitespace acts as delimiter for arguments,
	 *     except if written in single or double quotes
	 *  4. \ acts as escape inside and outside double quotes.
	 *     Inside double quotes, this is only useful for \".
	 *     Outside, it is useful for \', \", \\ and \ .
	 *  5. single quotes do not have an escape character
	 *  6. abort on "#" (comments)
	 */

	size_t i, j = 0;
	struct parsed_insn *ret = *line;

	for (i = 0; i <= len; ++i) {
		switch (state) {
		case ST_START:
			switch (buf[i]) {
			case ' ':
			case '\t':
				continue;
			case 0:
			case '#':
				break;
			case '\'':
				j = i+1;
				state = ST_SINGLE_QUOTES;
				break;
			case '"':
				j = i+1;
				state = ST_DOUBLE_QUOTES;
				break;
			default:
				j = i;
				state = ST_ARGUMENT;
				break;
			}
			/* prepare everything */
			ret = xcalloc(1, sizeof(struct parsed_insn));
			ret->line = lineno;
			strbuf_init(&ret->orig, len+2);
			if (!buf[i] || buf[i] == '#') /* empty/comment */
				goto finish;
			break;
		case ST_DELIMITER:
			switch (buf[i]) {
			case ' ':
			case '\t':
				continue;
			case 0:
				break;
			case '\'':
				j = i+1;
				state = ST_SINGLE_QUOTES;
				break;
			case '"':
				j = i+1;
				state = ST_DOUBLE_QUOTES;
				break;
			default:
				j = i;
				state = ST_ARGUMENT;
				if (buf[i] == '#') /* a comment */
					goto finish;
				break;
			}
			/* prepare next argument */
			ALLOC_GROW(ret->argv, ret->argc + 1, alloc);
			ret->argv[ret->argc++] = strbuf_detach(&arg_sb, NULL);
			break;
		case ST_ARGUMENT:
			switch (buf[i]) {
			case ' ':
			case '\t':
				strbuf_add(&arg_sb, buf+j, i-j);
				state = ST_DELIMITER;
				break;
			case '"':
				strbuf_add(&arg_sb, buf+j, i-j);
				j = i + 1;
				state = ST_DOUBLE_QUOTES;
				break;
			case '\'':
				strbuf_add(&arg_sb, buf+j, i-j);
				j = i + 1;
				state = ST_SINGLE_QUOTES;
				break;
			case '\\':
				strbuf_add(&arg_sb, buf+j, i-j);
				j = i + 1;
				state = ST_ESCAPE;
			default:
				break;
			}
			break;
		case ST_ESCAPE:
				state = ST_ARGUMENT;
			break;
		case ST_DOUBLE_QUOTES:
			switch (buf[i]) {
			case '"':
				strbuf_add(&arg_sb, buf+j, i-j);
				j = i + 1;
				state = ST_ARGUMENT;
				break;
			case '\\':
				strbuf_add(&arg_sb, buf+j, i-j);
				j = i + 1;
				state = ST_DOUBLE_QUOTES_ESCAPE;
				break;
			default:
				break;
			}
			break;
		case ST_DOUBLE_QUOTES_ESCAPE:
			state = ST_DOUBLE_QUOTES;
			break;
		case ST_SINGLE_QUOTES:
			switch (buf[i]) {
			case '\'':
				strbuf_add(&arg_sb, buf+j, i-j);
				j = i + 1;
				state = ST_ARGUMENT;
				break;
			default:
				break;
			}
			break;
		}
	}
finish:
	*line = ret;
	switch(state) {
	case ST_DOUBLE_QUOTES:
	case ST_DOUBLE_QUOTES_ESCAPE:
	case ST_SINGLE_QUOTES:
		strbuf_add(&arg_sb, buf+j, i-j-1);
		strbuf_add(&arg_sb, "\n", 1);
		return 1;
	case ST_ARGUMENT:
		if (i-j > 1)
			strbuf_add(&arg_sb, buf+j, i-j-1);
		ALLOC_GROW(ret->argv, ret->argc + 1, alloc);
		ret->argv[ret->argc++] = strbuf_detach(&arg_sb, NULL);
	case ST_DELIMITER:
		state = ST_START;
		alloc = 0;
	default:
		strbuf_addstr(&ret->orig, buf);
		strbuf_addch(&ret->orig, '\n');
		return 0;
	}
}

static void add_parsed_line_to_contents(struct parsed_insn *parsed_line)
{
	if (!contents.first) {
		contents.first = parsed_line;
		contents.last = parsed_line;
	} else {
		contents.last->next = parsed_line;
		contents.last = parsed_line;
	}
	if (parsed_line->argv)
		contents.total++;
}

/* Parse a file fp; write result into contents */
static void parse_file(const char *filename)
{
	struct strbuf str = STRBUF_INIT;
	struct parsed_insn *parsed_line = NULL;
	int r = 0;
	int lineno = 0;
	FILE *fp = fp = fopen(filename, "r");
	if (!fp)
		die("Could not open file '%s': %s", filename, strerror(errno));

	memset(&contents, 0, sizeof(struct parsed_file));

	while (strbuf_getline(&str, fp, '\n') != EOF) {
		lineno++;
		r = parse_line(str.buf, str.len, lineno, &parsed_line);
		if (!r)
			add_parsed_line_to_contents(parsed_line);
	}
	strbuf_release(&str);
	fclose(fp);
	if (r)
		die("Unexpected end of file.");
}


/**********************************************************************
 * Helpers for main execution and sanity check functions
 */

static void comment_for_reflog(const char *str)
{
	static char tmp[50];
	reflog = getenv("GIT_REFLOG_ACTION");
	if (!reflog || !*reflog) {
		char comment[35];
		char *space = strchr(str, ' ');
		if (space) {
			strlcpy(comment, str, MIN(space - str + 1,
						  sizeof(comment)-2));
			snprintf(tmp, sizeof(tmp), "sequencer (\"%s\")",
								comment);
		}
		else {
			strlcpy(comment, str, sizeof(comment));
			snprintf(tmp, sizeof(tmp), "sequencer (%s)", comment);
		}
		reflog = tmp;
	}
}

/* Test if working tree is dirty. */
static int is_working_tree_dirty(void)
{
	struct rev_info rev;
	int result;
	if (read_cache() < 0)
		return error("Could not read cache. Corrupted?");
	if (allow_dirty) /* don't check if --allow-dirty is used */
		return 0;
	if (refresh_cache(REFRESH_IGNORE_SUBMODULES))
		return 1;
	init_revisions(&rev, NULL);
	rev.abbrev = 40;
	DIFF_OPT_SET(&rev.diffopt, QUIET);
	DIFF_OPT_SET(&rev.diffopt, IGNORE_SUBMODULES);
	DIFF_OPT_SET(&rev.diffopt, EXIT_WITH_STATUS);
	rev.diffopt.output_format = DIFF_FORMAT_RAW;
	result = run_diff_files(&rev, 0);
	return diff_result_code(&rev.diffopt, result);
}

static void unlink_commit_files(void)
{
	if (file_exists(MERGE_MSG))
		unlink(MERGE_MSG);
	if (file_exists(MERGE_HEAD))
		unlink(MERGE_HEAD);
	if (file_exists(COMMIT_EDITMSG))
		unlink(COMMIT_EDITMSG);
	if (file_exists(PATCH_FILE))
		unlink(PATCH_FILE);
	if (file_exists(MESSAGE_FILE))
		unlink(MESSAGE_FILE);
}

static int remove_from_todo(void)
{
	FILE *fp;
	struct parsed_insn *cur;

	fp = fopen(DONE_FILE, "a");
	if (!fp)
		return error("Could not open file %s.", DONE_FILE);
	if (!fwrite(contents.cur->orig.buf, contents.cur->orig.len, 1, fp))
		return error("Could not write into %s.", DONE_FILE);
	fclose(fp);
	cur = contents.cur->next;

	fp = fopen(TODO_FILE, "w");
	if (!fp)
		return error("Could not open file %s.", TODO_FILE);
	for (; cur; cur = cur->next) {
		if (!fwrite(cur->orig.buf, cur->orig.len, 1, fp))
			return error("Could not write into %s.", TODO_FILE);
	}
	fclose(fp);

	if (contents.cur->argv) { /* we are not removing a comment */
		contents.count++;
		if ((verbosity && isatty(1)) || verbosity > 1) {
			printf("Sequencing (%zu/%zu)\r",
				contents.count, contents.total);
			if (verbosity > 1)
				putchar('\n');
		}
	}
	return 0;
}

static int parse_insn_options(int argc, const char **argv,
					const char ***newargv, int idx)
{
	struct parse_opt_ctx_t ctx;
	int i;
	const struct option *options = avail_insns[idx].options;
	const char * const *usagestr = avail_insns[idx].usage;
	*newargv = xcalloc(argc, sizeof(const char *));

	/* copy old arg into new arg, will be changed on parsing */
	for (i = 0; i < argc; ++i)
		(*newargv)[i] = argv[i];

	/* reset general options and insn options */
	memset(&opt, 0, sizeof(opt));
	if (avail_insns[idx].init)
		avail_insns[idx].init();

	/* do parsing */
	parse_options_start(&ctx, argc, *newargv, NULL, 0);
	switch (parse_options_step(&ctx, options, usagestr)) {
	case PARSE_OPT_DONE:
		break;
	case PARSE_OPT_HELP:
	default: /* PARSE_OPT_UNKNOWN */
		if (ctx.argv[0][1] == '-')
			todo_error("unknown option `%s'", ctx.argv[0] + 2);
		else
			todo_error("unknown switch `%c'", *ctx.opt);
		parse_options_usage(usagestr, options);
		return -1;
	}
	return parse_options_end(&ctx);
}


/**********************************************************************
 * Execution and sanity check of instructions
 */

static int check_insns(void)
{
	int ret = 0;

	for (contents.cur = contents.first; contents.cur;
	     contents.cur = contents.cur->next) {
		int i;
		int argc;
		const char **argv;

		if (!contents.cur->argv)
			continue;

		/* find instruction */
		i = find_insn(contents.cur->argv[0]);
		todo_line = contents.cur->line;
		if (i < 0) {
			ret |= todo_error("Unknown %s instruction.",
						contents.cur->argv[0]);
			continue;
		}

		/* parse options */
		argc = parse_insn_options(contents.cur->argc,
					  contents.cur->argv, &argv, i);
		if (argc < 0)
			return 1;

		/* run check */
		ret |= avail_insns[i].check(argc, argv);
		free(argv);
	}

	return ret;
}

static int execute_insns(void)
{
	int i;
	/* swap available_marks.nr and marks_total */
	i = available_marks.nr;
	available_marks.nr = marks_total;
	marks_total = i;

	for (contents.cur = contents.first; contents.cur;
	     contents.cur = contents.cur->next) {
		int ret = 0;
		int argc, oldargc;
		const char **argv;

		if (remove_from_todo())
			return error("Could not mark instruction as done");
		if (!contents.cur->argv)
			continue;

		reset_next_commit();
		save("sequencer.skiphead", "HEAD");

		if (verbosity > 2) {
			printf("Next command: %s\n", contents.cur->argv[0]);
			for (i = 1; i < contents.cur->argc; ++i)
				printf("   - '%s'\n", contents.cur->argv[i]);
		}

		/* find instruction */
		i = find_insn(contents.cur->argv[0]);

		/* parse options */
		oldargc = contents.cur->argc;
		argc = parse_insn_options(contents.cur->argc,
					  contents.cur->argv, &argv, i);

		/* update head_sha1 for insn */
		if (get_sha1("HEAD", head_sha1)) {
			free(argv);
			return error("You do not have a valid HEAD.");
		}

		unlink_commit_files();

		/* run */
		comment_for_reflog(avail_insns[i].usage[0]);
		ret = avail_insns[i].act(argc, argv);
		free(argv);

		if (ret) {
			error("%s instruction failed.", contents.cur->argv[0]);
			return 1;
		}
		free_commit_info(&next_commit);
	}

	comment_for_reflog("finish");

	if (orig_headname) {
		if (get_sha1("HEAD", head_sha1))
			return error("You do not have a valid HEAD.");
		if (update_ref(reflog, orig_headname, head_sha1, NULL, 0, 0))
			return error("Could not update %s to '%s'.",
					orig_headname, sha1_to_hex(head_sha1));
		if (create_symref("HEAD", orig_headname, NULL))
			return error("Could not update HEAD to %s.",
								orig_headname);
	}
	return 0;
}


/**********************************************************************
 * Different actions...
 */

int initial_checks(const char *act_name, int act_bit,
		   const char **act_caller)
{
	if (!file_exists(git_path(SEQ_DIR))) {
		error("No sequencer running. Cannot %s.", act_name);
		return 1;
	}
	reset_next_commit();
	if (get_saved_options())
		return 1;
	if (caller_check_failed & act_bit) {
		fprintf(stderr, "You must use '%s' to %s.\n",
			*act_caller, act_name);
		return 1;
	}
	comment_for_reflog(act_name);

	return 0;
}

/* Realize --abort */
static int sequencer_abort(int argc, const char **argv)
{
	if (initial_checks("abort", ACTION_ABORT, &caller_abort))
		return 1;
	rerere_clear();
	unlink_commit_files();
	if (restore())
		return 1;
	cleanup();
	return 0;
}

/* Realize --continue */
static int sequencer_continue(int argc, const char **argv)
{
	if (initial_checks("continue", ACTION_CONTINUE, &caller_continue))
		return 1;
	set_die_routine(die_continue_cb);
	parse_file(TODO_FILE);
	if (check_insns()) {
		why = WHY_TODO;
		die("TODO file contains errors.");
	}

	if (get_sha1("HEAD", head_sha1)) {
		error("You do not have a valid HEAD.");
		return 3;
	}
	if (read_cache() < 0) {
		error("Could not read the index.");
		return 3;
	}
	if (is_working_tree_dirty()) {
		error("Working tree is dirty. "
		      "(Use git-add or git-stash first?)");
		return 3;
	}

	/* Do we have anything to commit? (staged changes) */
	if (index_differs_from("HEAD", DIFF_OPT_IGNORE_SUBMODULES)) {
		unsigned char parent_sha1[20];
		unsigned char *tmp;

		/* prepare-msg-hook with "message"...  This is the way
		 * the original git-rebase --continue does it. */
		set_message_source("message");
		if (edit_message(0))
			return 3;

		if (file_exists(MERGE_HEAD)) { /* we are in a merge */
			struct strbuf m;
			FILE *fp;

			die_with_merges = 1;

			strbuf_init(&m, 0);
			fp = fopen(MERGE_HEAD, "r");
			if (fp == NULL)
				die("could not open %s for reading: %s",
				    MERGE_HEAD, strerror(errno));
			while (strbuf_getline(&m, fp, '\n') != EOF) {
				unsigned char sha1[20];
				if (get_sha1_hex(m.buf, sha1) < 0)
					die("Corrupt %s file (%s)",
						MERGE_HEAD, m.buf);
				string_list_append(xstrdup(sha1_to_hex(sha1)),
							&next_commit.parents);
			}
			fclose(fp);
			strbuf_release(&m);
			unlink(MERGE_HEAD);
		}

		/* After "pause" or "run", we should amend (merge-safe!).
		 * On conflict, we do not have a commit to amend, so we
		 * should just commit. */
		switch(why) {
		case WHY_PAUSE:
		case WHY_RUN:
			why = WHY_UNKNOWN;
			if (get_sha1("HEAD^1", parent_sha1))
				tmp = NULL; /* parent is root commit */
			else {
				if (hashcmp(head_sha1, paused_at_sha1))
					die("\nYou have uncommitted changes "
					    "in your working tree.\nPlease, "
					    "commit them first and try again.");
				tmp = parent_sha1;
			}
			if (do_commit_with_msgfile(tmp))
				die("Could not commit staged changes.");
			break;
		case WHY_CONFLICT:
			why = WHY_UNKNOWN;
			if (do_commit_with_msgfile(head_sha1))
				die("Could not commit staged changes.");
			add_comment_to_donefile("resolved conflicts of the last instruction");
			break;
		default:
			die("There are staged changes. "
			    "Do not know what to do with them.");
		}
		free_commit_info(&next_commit);
	}
	unlink_commit_files();
	die_with_merges = 0;

	why = WHY_UNKNOWN;
	if (execute_insns())
		die("Error executing instructions.");

	cleanup();
	return 0;
}

/* Realize --skip */
static int sequencer_skip(int argc, const char **argv)
{
	if (initial_checks("skip", ACTION_SKIP, &caller_skip))
		return 1;
	set_die_routine(die_continue_cb);
	parse_file(TODO_FILE);
	if (check_insns()) {
		why = WHY_TODO;
		die("TODO file contains errors.");
	}

	rerere_clear();
	unlink_commit_files();
	why = WHY_UNKNOWN;

	if (!skiphead)
		skiphead = "HEAD";

	if (get_sha1(skiphead, head_sha1)) {
		error("You do not have a valid HEAD to skip to.");
		return 3;
	}
	if (reset_almost_hard(head_sha1))
		return 3;

	add_comment_to_donefile("skipped the last instruction");

	if (execute_insns())
		die("Error executing instructions.");
	cleanup();
	return 0;
}

static int print_file_with_line_prefix(FILE *out, const char *filename,
					const char *prefix)
{
	struct strbuf line = STRBUF_INIT;
	FILE *fp  = fopen(filename, "r");
	int fail = 0;
	if (fp) {
		while (strbuf_getline(&line, fp, '\n') != EOF)
			fprintf(out, "%s%s\n", prefix, line.buf);
		strbuf_release(&line);
		fclose(fp);
	} else {
		fprintf(out, "%s-- Could not open '%s'.\n", prefix, filename);
		fail = 1;
	}
	return fail;
}

static int prepare_editable_todo(const char *filename, const char *markline)
{
	int result;
	FILE *fp = fopen(filename, "w");
	if (!fp)
		return error("Could not open '%s': %s",
					filename, strerror(errno));
	if (file_exists(DONE_FILE)) {
		fprintf(fp, "# ALREADY DONE:\n");
		print_file_with_line_prefix(fp, DONE_FILE, "#  ");
		fprintf(fp, "# \n");
	}
	fprintf(fp, "%s\n", markline);
	result = print_file_with_line_prefix(fp, TODO_FILE, "");
	fclose(fp);
	return result;
}

static int rewind_prepared_todo(const char *filename, const char *markline)
{
	int marked = 0;
	struct strbuf buf = STRBUF_INIT;
	FILE *ifp = fopen(filename, "r");
	FILE *ofp = fopen(TODO_FILE, "w");
	if (!ifp)
		return error("Could not open TODO file %s: %s",
					filename, strerror(errno));
	if (!ofp)
		return error("Could not open TODO file %s: %s",
					TODO_FILE, strerror(errno));
	while (strbuf_getline(&buf, ifp, '\n') != EOF) {
		if (buf.buf && (marked || buf.buf[0] != '#')) {
			puts(buf.buf);
			fprintf(ofp, "%s\n", buf.buf);
		} else if (!strcmp(buf.buf, markline))
			marked = 1;
	}
	fclose(ifp);
	fclose(ofp);
	strbuf_release(&buf);
	return 0;
}

/* Realize --edit */
static int sequencer_edit(int argc, const char **argv)
{
	const char *markline = "### BEGIN EDITING BELOW THIS LINE ###";
	char *todo_edit_file, *todo_backup_file;
	if (!file_exists(git_path(SEQ_DIR)))
		die("No sequencer running.");
	if (get_saved_options())
		return 1;
	todo_edit_file = git_pathdup(SEQ_DIR "/git-rebase-todo");
	if (prepare_editable_todo(todo_edit_file, markline))
		return error("Could not prepare TODO file.");

	if (launch_editor(todo_edit_file, NULL, NULL))
		return 1;

	if (isatty(0) && isatty(1)) { /* interactive */
		putchar('\n');
		parse_file(todo_edit_file);
		while (!contents.total || check_insns()) {
			char reply;
			struct parsed_insn *cur;
			if (!contents.total)
				puts("TODO file is empty.");
			printf("What to do with the file? "
				"[c]orrect/[e]dit again/[r]ewind/[s]ave/[?] ");

			/* read character */
			do
				scanf("%c", &reply);
			while (!isalpha(reply) && reply != '?');

			switch (reply) {
			case 'c': case 'C': /* correct */
				if (launch_editor(todo_edit_file, NULL, NULL))
					return 1;
				break;
			case 'e': case 'E': /* edit again */
				if (prepare_editable_todo(todo_edit_file,
							  markline))
					return 1;
				if (launch_editor(todo_edit_file, NULL, NULL))
					return 1;
				break;
			case 'r': case 'R': /* rewind */
			case 'x': case 'X': case 'q': case 'Q': /* exit/quit */
				/* x, X, q and Q are for the confused user,
				 * who just wants to exit. */
				unlink(todo_edit_file);
				return 0;
			case 's': case 'S':
				if (why == WHY_PAUSE)
					save_int("sequencer.why", WHY_TODO);
				goto force_finish;
			case '?': case 'h': case 'H':
				printf("\n\nHelp:\n"
				"s - save TODO file and exit\n"
				"c - respawn editor to correct TODO file\n"
				"e - drop changes and respawn editor on "
							"original TODO file\n"
				"r - drop changes and exit as if nothing "
								"happened\n"
				"? - print this help\n");
				break;
			default:
				putchar('\n');
			}

			/* parse again */
			free_available_marks();
			for (cur = contents.first; cur; free_parsed_insn(&cur));
			parse_file(todo_edit_file);
		}
	} else { /* non-interactive, default */
		parse_file(todo_edit_file);
		if (!contents.total) {
			puts("Nothing to do.");
			return 0;
		}
		if (check_insns())
			die("TODO file still contains errors. Aborting.");
	}
force_finish:
	/* backup old TODO file */
	todo_backup_file = git_pathdup(SEQ_DIR "/todo.old");
	if (copy_file_overwrite(todo_backup_file, TODO_FILE))
		warning("Could not backup TODO file to '%s'.", *argv);
	free(todo_backup_file);

	/* remove markline and comment lines above and print TODO file */
	printf("TODO file contains:\n\n");
	if (rewind_prepared_todo(todo_edit_file, markline))
		return 1;
	unlink(todo_edit_file);
	free(todo_edit_file);
	return 0;
}

/* Realize --status */
static int sequencer_status(int argc, const char **argv)
{
	if (!file_exists(git_path(SEQ_DIR)))
		die("No sequencer running.");
	if (get_saved_options())
		return 1;
	if (file_exists(DONE_FILE)) {
		puts("Already done (or tried):");
		print_file_with_line_prefix(stdout, DONE_FILE, "  ");
		putchar('\n');
	}
	switch (why) {
	case WHY_PAUSE:
		puts("Intentionally paused.");
		break;
	case WHY_RUN:
		puts("Interrupted because a 'run' instruction failed.");
		break;
	case WHY_CONFLICT:
		puts("Interrupted by conflict at last line above.");
		break;
	case WHY_TODO:
		puts("Interrupted because of errors in the TODO file.");
		break;
	default:
		puts("Current state is broken.");
	}
	if (verbosity > 1)
		puts("\nRunning verbosely.");
	if (!verbosity)
		puts("\nRunning quietly.");
	if (orig_headname) {
		char *name = strrchr(orig_headname, '/');
		printf("\nSequencing on branch '%s'.\n",
						name ? name+1 : orig_headname);
	}
	if (file_exists(TODO_FILE)) {
		puts("\nStill to do:");
		print_file_with_line_prefix(stdout, TODO_FILE, "  ");
	}
	if (why == WHY_TODO)
		printf(	"\n"
			"But there are errors. To edit, run:\n"
			"\tgit sequencer --edit\n");
	else {
		printf(	"\n"
			"To abort & restore, invoke:\n"
			"\t%s\n", caller_abort);
		printf(	"To continue, invoke:\n"
			"\t%s\n", caller_continue);
		/* --skip would only lead to confusion, on "pause" or "run" */
		if (why != WHY_PAUSE && why != WHY_RUN)
			printf(	"To skip the current instruction, invoke:\n"
				"\t%s\n", caller_skip);
	}
	return 0;
}

/* Realize initial invocation */
static int sequencer_startup(int argc, const char **argv)
{
	if (file_exists(git_path(SEQ_DIR)))
		die("Sequencer already started.");

	if (get_sha1("HEAD", head_sha1))
		die("You do not have a valid HEAD.");
	if (is_working_tree_dirty())
		die("Working tree is dirty. You can try --allow-dirty.");
	if (index_differs_from("HEAD", DIFF_OPT_IGNORE_SUBMODULES))
		die("Index is dirty.");

	if (mkdir(git_path(SEQ_DIR), 0777))
		die("Could not create temporary %s.", git_path(SEQ_DIR));

	set_die_routine(die_abort_cb);

	comment_for_reflog("start");

	set_verbosity(verbosity);
	save_int("sequencer.verbosity", verbosity);
	save("sequencer.advice", advice ? "true" : "false");
	save("sequencer.allowdirty", allow_dirty ? "true" : "false");

	save("sequencer.caller.abort", caller_abort);
	save("sequencer.caller.continue", caller_continue);
	save("sequencer.caller.skip", caller_skip);

	/* save old head before checking out the given <branch> */
	orig_headname = resolve_symbolic_ref("HEAD");
	if (orig_headname && !prefixcmp(orig_headname, "refs/heads/")) {
		/* save branch name and detach head */
		save("sequencer.headname", orig_headname);
		if (update_ref(reflog, "HEAD", head_sha1, head_sha1,
							REF_NODEREF, 0))
			die("Could not detach HEAD.");
	} else
		orig_headname = NULL;
	save("sequencer.head", sha1_to_hex(head_sha1));
	hashcpy(orig_head_sha1, head_sha1);

	if (argc) {
		if (copy_file(TODO_FILE, *argv, 0666))
			die("Could not copy TODO file '%s'.", *argv);
	} else {
		/* read from stdin and write into TODO_FILE */
		int fd;
		fd = open(TODO_FILE, O_WRONLY | O_CREAT, 0666);
		if (fd < 0)
			die("Could not open file '%s' for writing.", TODO_FILE);
		if (copy_fd(fileno(stdin), fd))
			die("Could not append to TODO file '%s'.", TODO_FILE);
		close(fd);
	}

	parse_file(TODO_FILE);
	if (!contents.total)
		die("Nothing to do.");

	why = WHY_UNKNOWN;
	memset(&available_marks, 0, sizeof(struct string_list));
	set_die_routine(die_continue_cb);
	if (check_insns()) {
		why = WHY_TODO;
		die("TODO file contains errors.");
	}

	if (execute_insns())
		die("Error executing instructions.");
	cleanup();
	return 0;
}

int cmd_sequencer(int argc, const char **argv, const char *prefix)
{
	const char * const seq_usage[] = {
		"git sequencer [options] [--] [<file>]",
		"git sequencer (--continue | --abort | --skip)",
		"git sequencer (--edit | --status)",
		NULL
	};
	int action = 0;
	struct option seq_options[] = {
		OPT_GROUP("Options to start a sequencing process"),
		OPT_BOOLEAN(0, "allow-dirty", &allow_dirty,
			"run even if working tree is dirty"),
		OPT_BOOLEAN('B', "batch", &batchmode, "run in batch-mode"),
		OPT__VERBOSE(&verbosity),
		OPT_SET_INT(0, "no-advice", &advice,
			"suppress advice when pausing (conflicts, etc)", 0),
		OPT_SET_INT('q', "quiet", &verbosity, "suppress output", 0),
		OPT_GROUP("Options to restart/change a sequencing process "
			  "or show information"),
		OPT_BIT(0, "continue", &action,
			"continue paused sequencer process", ACTION_CONTINUE),
		OPT_BIT(0, "abort", &action,
			"restore original branch and abort", ACTION_ABORT),
		OPT_BIT(0, "skip", &action,
			"skip current patch and continue", ACTION_SKIP),
		OPT_BIT(0, "status", &action,
			"show the status of the sequencing process",
							ACTION_STATUS),
		OPT_BIT(0, "edit", &action,
			"invoke editor to let user edit the remaining insns",
								ACTION_EDIT),
		OPT_GROUP("Options to be used by user scripts"),
		OPT_CALLBACK(0, "caller", NULL, "infostring",
			"provide information string: name|abort|cont|skip",
			prepare_caller_strings),
		OPT_END(),
	};

	refresh_cache(REFRESH_QUIET);
	if (unmerged_cache())
		die("You need to resolve your current index first.");

	git_config(git_default_config, NULL);

	git_committer_info(IDENT_ERROR_ON_NO_NAME);

	prepare_caller_strings(NULL, NULL, 1);
	argc = parse_options(argc, argv, NULL, seq_options, seq_usage, 0);

	if (action) {
		if (argc || batchmode || allow_dirty || advice-1 || verbosity-1)
			usage_with_options(seq_usage, seq_options);

		switch (action) {
		case ACTION_ABORT:
			return sequencer_abort(argc, argv);
		case ACTION_CONTINUE:
			return sequencer_continue(argc, argv);
		case ACTION_SKIP:
			return sequencer_skip(argc, argv);
		case ACTION_STATUS:
			return sequencer_status(argc, argv);
		case ACTION_EDIT:
			return sequencer_edit(argc, argv);
		default:
			usage_with_options(seq_usage, seq_options);
		}
	} else if (argc <= 1)
		return sequencer_startup(argc, argv);
	else
		usage_with_options(seq_usage, seq_options);
}
