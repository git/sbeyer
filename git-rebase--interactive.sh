#!/bin/sh
#
# Copyright (c) 2006 Johannes E. Schindelin

# SHORT DESCRIPTION
#
# This script makes it easy to fix up commits in the middle of a series,
# and rearrange commits.
#
# The original idea comes from Eric W. Biederman, in
# http://article.gmane.org/gmane.comp.version-control.git/22407

OPTIONS_KEEPDASHDASH=
OPTIONS_SPEC="\
git-rebase [-i] [options] [--] <upstream> [<branch>]
git-rebase [-i] (--continue | --abort | --skip)
--
 Available options are
v,verbose          display a diffstat of what changed upstream
onto=              rebase onto given branch instead of upstream
p,preserve-merges  try to recreate merges instead of ignoring them
s,strategy=        use the given merge strategy
m,merge            always used (no-op)
i,interactive      always used (no-op)
 Actions:
continue           continue rebasing process
abort              abort rebasing process and restore original branch
skip               skip current patch and continue rebasing process
no-verify          override pre-rebase hook from stopping the operation
root               rebase all reachable commmits up to the root(s)
"

. git-sh-setup
require_work_tree

DOTEST="$GIT_DIR/rebase-merge"
TODO="$DOTEST"/git-rebase-todo
DONE="$DOTEST"/done
MSG="$DOTEST"/message
SQUASH_MSG="$DOTEST"/message-squash
REWRITTEN="$DOTEST"/rewritten
DROPPED="$DOTEST"/dropped
PRESERVE_MERGES=
STRATEGY=
ONTO=
VERBOSE=
OK_TO_SKIP_PRE_REBASE=
REBASE_ROOT=

warn () {
	echo "$*" >&2
}

output () {
	case "$VERBOSE" in
	'')
		output=$("$@" 2>&1 )
		status=$?
		test $status != 0 && printf "%s\n" "$output"
		return $status
		;;
	*)
		"$@"
		;;
	esac
}

run_pre_rebase_hook () {
	if test -z "$OK_TO_SKIP_PRE_REBASE" &&
	   test -x "$GIT_DIR/hooks/pre-rebase"
	then
		"$GIT_DIR/hooks/pre-rebase" ${1+"$@"} || {
			echo >&2 "The pre-rebase hook refused to rebase."
			exit 1
		}
	fi
}

require_clean_work_tree () {
	# test if working tree is dirty
	git rev-parse --verify HEAD > /dev/null &&
	git update-index --ignore-submodules --refresh &&
	git diff-files --quiet --ignore-submodules &&
	git diff-index --cached --quiet HEAD --ignore-submodules -- ||
	die "Working tree is dirty"
}

die_abort () {
	rm -rf "$DOTEST"
	die "$1"
}

has_action () {
	grep '^[^#]' "$1" > /dev/null
}

create_todo_line_preserving_merges () {
	shortsha1=$sha1
	sha1=$(git rev-parse $sha1)

	preserve=t
	test -z "$REBASE_ROOT" || preserve=f
	new_parents=
	first_parent=
	pend=" $(git rev-list --parents -1 $sha1 | cut -d' ' -s -f2-)"
	if test "$pend" = " "
	then
		pend=" root"
	fi
	while [ "$pend" != "" ]
	do
		p=$(expr "$pend" : ' \([^ ]*\)')
		pend="${pend# $p}"

		# check if we've already seen this parent
		if test -f "$REWRITTEN"/$p
		then
			preserve=f
			new_p=$(cat "$REWRITTEN"/$p)
			case "$new_parents" in
			*$new_p*)
				;; # do nothing; that parent is already there
			*)
				new_parents="$new_parents $new_p"
				;;
			esac
		else
			if test -f "$DROPPED"/$p
			then
				replacement="$(cat "$DROPPED"/$p)"
				test -z "$replacement" && replacement=root
				pend=" $replacement$pend"
			else
				new_parents="$new_parents $p"
			fi
		fi
		test -n "$first_parent" || first_parent=$p
	done
	# We do not have parent, so ignore this commit
	test t = $preserve && return

	# We always write a mark, because we do not know if there will
	# be a "reset" or "merge".
	# Filter the unneeded marks out afterwards.
	echo "mark :$mark"
	mark=$(($mark+1))

	new_first_parent=$(expr "$new_parents" : ' \([^ ]*\)')

	# Reset if needed
	test -z "$first_parent" -o "$first_parent" = $lastsha1 || {
		if expr $new_first_parent : ^: > /dev/null
		then
			echo "reset $new_first_parent"
		else
			git rev-list --pretty=format:"reset %h	# %s" \
					$new_first_parent'^!' | sed -e 1d
		fi
	}

	echo ":$mark" > "$REWRITTEN"/$sha1

	# Merge or pick
	case "$new_parents" in
	' '*' '*)
		new_parents=${new_parents# $new_first_parent}
		remotes=
		# new_parents is a list of all new parents.  But we only want
		# remotes that have not already merged in.
		for p in $new_parents
		do
			if expr $p : ^: > /dev/null ||
			   ! git rev-list $ONTO | grep $p > /dev/null
			then
				remotes=" $p"
			fi
		done
		test -n "$remotes" || return # No remotes? Ignore this commit!
		printf 'merge%s -C %s%s\t%s\n' "$STRATEGY" \
			"$shortsha1" "$remotes" "$rest"
		;;
	*)
		printf 'pick %s\t%s\n' "$shortsha1" "$rest"
		;;
	esac

	lastsha1="$sha1"
	return 0
}

update_refs_and_exit () {
	HEADNAME=$(cat "$DOTEST"/head-name) &&
	OLDHEAD=$(cat "$DOTEST"/head) &&
	SHORTONTO=$(git rev-parse --short $(cat "$DOTEST"/onto)) &&
	NEWHEAD=$(git rev-parse HEAD) &&
	case $HEADNAME in
	refs/*)
		message="$GIT_REFLOG_ACTION: $HEADNAME onto $SHORTONTO" &&
		git update-ref -m "$message" $HEADNAME $NEWHEAD $OLDHEAD &&
		git symbolic-ref HEAD $HEADNAME
		;;
	esac && {
		test ! -f "$DOTEST"/verbose ||
			git diff-tree --stat $(cat "$DOTEST"/head)..HEAD
	} &&
	rm -rf "$DOTEST" &&
	git gc --auto &&
	warn "Successfully rebased and updated $HEADNAME."

	exit
}

# check if no other options are set
is_standalone () {
	test $# -eq 2 -a "$2" = '--' &&
	test -z "$ONTO" &&
	test -z "$PRESERVE_MERGES" &&
	test -z "$STRATEGY" &&
	test -z "$VERBOSE"
}

get_saved_options () {
	test -d "$REWRITTEN" && PRESERVE_MERGES=t
	test -f "$DOTEST"/strategy && STRATEGY="$(cat "$DOTEST"/strategy)"
	test -f "$DOTEST"/verbose && VERBOSE=t
	test -f "$DOTEST"/rebase-root && REBASE_ROOT=t
}

run_sequencer () {
	git sequencer --caller='git rebase -i|--abort|--continue|--skip' "$@"
	case "$?" in
	0)
		if test "$1" = --abort
		then
			HEADNAME=$(cat "$DOTEST"/head-name)
			HEAD=$(cat "$DOTEST"/head)
			case $HEADNAME in
			refs/*)
				git symbolic-ref HEAD $HEADNAME
				;;
			esac &&
			output git reset --hard $HEAD &&
			rm -rf "$DOTEST"
			exit
		else
			update_refs_and_exit
		fi
		;;
	2)
		# pause
		exit 0
		;;
	3)
		# conflict
		exit 1
		;;
	*)
		die_abort 'git-sequencer died unexpected.'
		;;
	esac
}

while test $# != 0
do
	case "$1" in
	--no-verify)
		OK_TO_SKIP_PRE_REBASE=yes
		;;
	--verify)
		;;
	--abort|--continue|--skip)
		is_standalone "$@" || usage
		run_sequencer "$1"
		;;
	-s)
		case "$#,$1" in
		*,*=*)
			STRATEGY=" -s "$(expr "z$1" : 'z-[^=]*=\(.*\)') ;;
		1,*)
			usage ;;
		*)
			STRATEGY=" -s $2"
			shift ;;
		esac
		;;
	-m)
		# we use merge anyway
		;;
	-v)
		VERBOSE=t
		;;
	-p)
		PRESERVE_MERGES=t
		;;
	-i)
		# yeah, we know
		;;
	--root)
		REBASE_ROOT=t
		;;
	--onto)
		shift
		ONTO=$(git rev-parse --verify "$1") ||
			die "Does not point to a valid commit: $1"
		;;
	--)
		shift
		test -z "$REBASE_ROOT" -a $# -ge 1 -a $# -le 2 ||
		test ! -z "$REBASE_ROOT" -a $# -le 1 || usage
		test -d "$DOTEST" &&
			die "Interactive rebase already started"
		git sequencer --status > /dev/null 2>&1 &&
			die "Sequencer already started. Cannot run rebase."

		git var GIT_COMMITTER_IDENT > /dev/null ||
			die "You need to set your committer info first"

		if test -z "$REBASE_ROOT"
		then
			UPSTREAM_ARG="$1"
			UPSTREAM=$(git rev-parse --verify "$1") || die "Invalid base"
			test -z "$ONTO" && ONTO=$UPSTREAM
			shift
		else
			UPSTREAM=
			UPSTREAM_ARG=--root
			test -z "$ONTO" &&
				die "You must specify --onto when using --root"
		fi
		run_pre_rebase_hook "$UPSTREAM_ARG" "$@"

		require_clean_work_tree

		if test ! -z "$1"
		then
			output git show-ref --verify --quiet "refs/heads/$1" ||
				die "Invalid branchname: $1"
			output git checkout "$1" ||
				die "Could not checkout $1"
		fi

		HEAD=$(git rev-parse --verify HEAD) || die "No HEAD?"
		mkdir "$DOTEST" || die "Could not create temporary $DOTEST"

		: > "$DOTEST"/interactive || die_abort "Could not mark as interactive"
		git symbolic-ref HEAD > "$DOTEST"/head-name 2> /dev/null ||
			echo "detached HEAD" > "$DOTEST"/head-name

		echo $HEAD > "$DOTEST"/head
		case "$REBASE_ROOT" in
		'')
			rm -f "$DOTEST"/rebase-root ;;
		*)
			: >"$DOTEST"/rebase-root ;;
		esac
		echo $ONTO > "$DOTEST"/onto
		test -z "$STRATEGY" || echo "$STRATEGY" > "$DOTEST"/strategy
		test t = "$VERBOSE" && : > "$DOTEST"/verbose

		SHORTHEAD=$(git rev-parse --short $HEAD)
		SHORTONTO=$(git rev-parse --short $ONTO)
		if test -z "$REBASE_ROOT"
			# this is now equivalent to ! -z "$UPSTREAM"
		then
			SHORTUPSTREAM=$(git rev-parse --short $UPSTREAM)
			REVISIONS=$UPSTREAM...$HEAD
			SHORTREVISIONS=$SHORTUPSTREAM..$SHORTHEAD
		else
			REVISIONS=$ONTO...$HEAD
			SHORTREVISIONS=$SHORTHEAD
		fi

		if test t = "$PRESERVE_MERGES"
		then
			lastsha1=
			# $REWRITTEN contains files for each commit that is
			# reachable on the way between $UPSTREAM and $HEAD.
			# The filename is the SHA1 of the old value and the
			# content is the SHA1 or :mark of the new one.
			if test -z "$REBASE_ROOT"
			then
				mkdir "$REWRITTEN" &&
				for c in $(git merge-base --all $HEAD $UPSTREAM)
				do
					test -n "$lastsha1" || lastsha1=$c
					echo $ONTO > "$REWRITTEN"/$c ||
						die "Could not init rewritten commits"
				done
			else
				mkdir "$REWRITTEN" &&
				echo $ONTO > "$REWRITTEN"/root ||
					die "Could not init rewritten commits"
			fi

			git rev-list \
				--pretty=format:"%m%h	# %s" --topo-order \
				--reverse --cherry-pick $REVISIONS | \
				sed -n -e "s/^>//p" > "$DOTEST"/commit-list

			mkdir "$DROPPED"

			mark=0
			# drop the --cherry-pick parameter this time
			git rev-list \
				--pretty=format:"%m%h	# %s" --topo-order \
				--reverse $REVISIONS | \
				sed -n -e "s/^>//p" | \
				while read -r sha1 rest
			do
				grep --quiet "$sha1" "$DOTEST"/commit-list
				if [ $? -eq 0 ]
				then
					# The current commit is not dropped by
					# --cherry-pick, so create a TODO line
					create_todo_line_preserving_merges
				else
					# The current commit has been dropped
					# so put its first parent into
					# $DROPPED/$fullsha1
					full=$(git rev-parse $sha1)
					git rev-list --parents -1 $sha1 | \
						cut -d' ' -s -f2 > "$DROPPED"/$full
					# Use -f2 because if rev-list is
					# telling this commit is not
					# worthwhile, we don't want to track
					# its multiple heads, just the history
					# of its first-parent for others that
					# will be rebasing on top of us
				fi
			done > "$TODO"

			# We now have more "mark :..." lines than needed.
			# Remove the unused.  This is just a step to keep
			# the list clean.
			keep_marks=$(sed -e "/^mark :/d" <"$TODO" |
				sed -n -e 's/^[^#]* :\([0-9][0-9]*\).*$/:\1:/p')
			while read -r line
			do
				case "$line" in
				'mark :'*)
					case "$keep_marks " in
					*${line#mark }:*)
						echo "$line"
						;;
					esac
					;;
				*)
					printf '%s\n' "$line"
					;;
				esac
			done < "$TODO" > "$TODO".new
			mv "$TODO".new "$TODO"
		else
			git rev-list --no-merges \
				--pretty=format:"%mpick %h	# %s" \
				--reverse --cherry-pick $REVISIONS | \
				sed -n -e "s/^>//p" > "$TODO"
		fi

		test -s "$TODO" || echo noop >> "$TODO"
		cat >> "$TODO" << EOF

# Rebase $SHORTREVISIONS onto $SHORTONTO
#
# Commands:
#  p, pick = use commit
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#
# If you remove a line here THAT COMMIT WILL BE LOST.
# However, if you remove everything, the rebase will be aborted.
#
EOF

		has_action "$TODO" ||
			die_abort "Nothing to do"

		cp "$TODO" "$TODO".backup
		git_editor "$TODO" ||
			die "Could not execute editor"

		has_action "$TODO" ||
			die_abort "Nothing to do"

		git update-ref ORIG_HEAD $HEAD
		output git checkout $ONTO && run_sequencer "$TODO"
		;;
	esac
	shift
done
