#!/bin/sh
#
# Copyright (c) 2005, 2006 Junio C Hamano

SUBDIRECTORY_OK=Yes
OPTIONS_KEEPDASHDASH=
OPTIONS_SPEC="\
git am [options] [<mbox>|<Maildir>...]
git am [options] (--resolved | --skip | --abort)
--
i,interactive   run interactively
b,binary*       (historical option -- no-op)
3,3way          allow fall back on 3way merging if needed
q,quiet         be quiet
s,signoff       add a Signed-off-by line to the commit message
u,utf8          recode into utf8 (default)
k,keep          pass -k flag to git-mailinfo
whitespace=     pass it through git-apply
directory=      pass it through git-apply
C=              pass it through git-apply
p=              pass it through git-apply
patch-format=   format the patch(es) are in
reject          pass it through git-apply
resolvemsg=     override error message when patch failure occurs
r,resolved      to be used after a patch failure
skip            skip the current patch
abort           restore the original branch and abort the patching operation.
committer-date-is-author-date    lie about committer date
ignore-date     use current timestamp for author date
rebasing*       (internal use for git-rebase)"

. git-sh-setup
prefix=$(git rev-parse --show-prefix)
set_reflog_action am
require_work_tree
cd_to_toplevel

git var GIT_COMMITTER_IDENT >/dev/null ||
	die "You need to set your committer info first"

cleanup () {
	git gc --auto
	rm -fr "$dotest"
}

die_abort () {
	cleanup
	die "$1"
}

stop_here_user_resolve () {
    if [ -n "$resolvemsg" ]; then
	    printf '%s\n' "$resolvemsg"
	    exit 1
    fi
    cmdline="git am"
    if test '' != "$interactive"
    then
        cmdline="$cmdline -i"
    fi
    if test '' != "$threeway"
    then
        cmdline="$cmdline -3"
    fi
    echo "When you have resolved this problem run \"$cmdline --resolved\"."
    echo "If you would prefer to skip this patch, instead run \"$cmdline --skip\"."
    echo "To restore the original branch and stop patching run \"$cmdline --abort\"."

    exit 1
}


be_interactive () {
	msg="$GIT_DIR/sequencer/message"
	patch="$GIT_DIR/sequencer/patch"
	# we rely on sequencer here

	test -t 0 ||
		die "cannot be interactive without stdin connected to a terminal."
	action=$(cat "$dotest/interactive")
	while test "$action" = again
	do
		echo
		echo "Commit Body is:"
		echo "--------------------------"
		cat "$msg"
		echo "--------------------------"
		if test -z "$1"
		then
			printf "Apply? [y]es/[n]o/[e]dit/[v]iew patch/[a]ccept all "
		else
			echo 'Patch does not apply cleanly!'
			printf "Apply+fix? [y]es/[n]o/[e]dit/[v]iew patch/[a]ccept all "
		fi

		read reply
		case "$reply" in
		[yY]*)
			return 0
			;;
		[nN]*)
			# pretend we never tried to apply
			to=HEAD
			test conflict = "$1" ||
				to=HEAD^
			git read-tree -m -u HEAD $to
			git reset -q $to >/dev/null
			return 1
			;;
		[eE]*)
			git_editor "$msg"
			git commit --amend --file="$msg" --no-verify >/dev/null
			;;
		[vV]*)
			LESS=-S ${PAGER:-less} "$patch"
			;;
		[aA]*)
			echo 'accept' >"$dotest/interactive"
			return 0
			;;
		*)
			:
			;;
		esac
	done
	test "$action" = accept &&
		sed -n -e '1s/^/Applying &/p' <"$msg"
	return 0
}

run_sequencer () {
	output=$(git sequencer $seqopts \
		--caller='git am|--abort|--resolved|--skip' "$@")
	ret=$?
	test -z "$output" || printf '%s\n' "$output"
	# we want this "printf" stuff to suppress sequencer's
	# "Sequencing..." messages
	case "$ret" in
	0)
		cleanup
		exit 0
		;;
	2|3)
		stop_here_user_resolve
		;;
	*)
		die_abort 'git-sequencer died unexpected. Aborting.'
		;;
	esac
}

run_sequencer_i () {
	command="$1"
	while true
	do
		output=$(git sequencer $seqopts \
			--caller='git am -i|--abort|--resolved|--skip' \
			$command 2>&1)
		case "$?" in
		0)
			cleanup
			exit 0
			;;
		2)
			if test -f "$dotest/conflict"
			then
				rm "$dotest/conflict"
			else
				be_interactive
			fi
			;;
		3)
			: >"$dotest/conflict"
			be_interactive conflict
			if test $? -eq 0
			then
				printf '%s\n' "$output" 1>&2
				stop_here_user_resolve
			fi
			;;
		*)
			die_abort "$output"
			;;
		esac
		seqopts=
		command=--continue
	done
}

escape_double_quotes () {
	printf %s "$*" | sed -e 's/\\/\\\\/g' -e 's/"/\\"/g'
}

clean_abort () {
	test $# = 0 || echo >&2 "$@"
	rm -fr "$dotest"
	exit 1
}

patch_format=

check_patch_format () {
	# early return if patch_format was set from the command line
	if test -n "$patch_format"
	then
		return 0
	fi

	# we default to mbox format if input is from stdin and for
	# directories
	if test $# = 0 || test "x$1" = "x-" || test -d "$1"
	then
		patch_format=mbox
		return 0
	fi

	# otherwise, check the first few lines of the first patch to try
	# to detect its format
	{
		read l1
		read l2
		read l3
		case "$l1" in
		"From "* | "From: "*)
			patch_format=mbox
			;;
		'# This series applies on GIT commit'*)
			patch_format=stgit-series
			;;
		"# HG changeset patch")
			patch_format=hg
			;;
		*)
			# if the second line is empty and the third is
			# a From, Author or Date entry, this is very
			# likely an StGIT patch
			case "$l2,$l3" in
			,"From: "* | ,"Author: "* | ,"Date: "*)
				patch_format=stgit
				;;
			*)
				;;
			esac
			;;
		esac
	} < "$1" || clean_abort
}

split_patches () {
	case "$patch_format" in
	mbox)
		git mailsplit -d"$prec" -o"$dotest" -b -- "$@" > "$dotest/last" ||
		clean_abort
		;;
	stgit-series)
		if test $# -ne 1
		then
			clean_abort "Only one StGIT patch series can be applied at once"
		fi
		series_dir=`dirname "$1"`
		series_file="$1"
		shift
		{
			set x
			while read filename
			do
				set "$@" "$series_dir/$filename"
			done
			# remove the safety x
			shift
			# remove the arg coming from the first-line comment
			shift
		} < "$series_file" || clean_abort
		# set the patch format appropriately
		patch_format=stgit
		# now handle the actual StGIT patches
		split_patches "$@"
		;;
	stgit)
		this=0
		for stgit in "$@"
		do
			this=`expr "$this" + 1`
			msgnum=`printf "%0${prec}d" $this`
			# Perl version of StGIT parse_patch. The first nonemptyline
			# not starting with Author, From or Date is the
			# subject, and the body starts with the next nonempty
			# line not starting with Author, From or Date
			perl -ne 'BEGIN { $subject = 0 }
				if ($subject > 1) { print ; }
				elsif (/^\s+$/) { next ; }
				elsif (/^Author:/) { print s/Author/From/ ; }
				elsif (/^(From|Date)/) { print ; }
				elsif ($subject) {
					$subject = 2 ;
					print "\n" ;
					print ;
				} else {
					print "Subject: ", $_ ;
					$subject = 1;
				}
			' < "$stgit" > "$dotest/$msgnum" || clean_abort
		done
		echo "$this" > "$dotest/last"
		this=
		msgnum=
		;;
	*)
		clean_abort "Patch format $patch_format is not supported."
		;;
	esac
}

prec=4
dotest="$GIT_DIR/rebase-apply"
todofile="$dotest/todo"
sign= utf8=t keep= skip= interactive= resolved= rebasing= abort=
resolvemsg=
opts=
committer_date_is_author_date=
ignore_date=

while test $# != 0
do
	case "$1" in
	-i|--interactive)
		interactive=_i ;;
	-b|--binary)
		: ;;
	-3|--3way)
		threeway=t ;;
	-s|--signoff)
		sign=t ;;
	-u|--utf8)
		utf8=t ;; # this is now default
	--no-utf8)
		utf8= ;;
	-k|--keep)
		keep=t ;;
	-r|--resolved)
		resolved=t ;;
	--skip)
		skip=t ;;
	--abort)
		abort=t ;;
	--rebasing)
		rebasing=t threeway=t keep=t ;;
	-d|--dotest)
		die "-d option is no longer supported.  Do not use."
		;;
	--resolvemsg)
		shift; resolvemsg=$1 ;;
	--whitespace|--directory)
		opts="$opts $1=\"$(escape_double_quotes $2)\""; shift ;;
	-C)
		opts="$opts --context=$2" ; shift ;;
	-p)
		opts="$opts $1$2"; shift ;;
	--patch-format)
		shift ; patch_format="$1" ;;
	--reject)
		opts="$opts $1" ;;
	--committer-date-is-author-date)
		committer_date_is_author_date=t ;;
	--ignore-date)
		ignore_date=t ;;
	-q|--quiet)
		GIT_QUIET=--quiet ;;
	--)
		shift; break ;;
	*)
		usage ;;
	esac
	shift
done

if test -d "$dotest"
then
	test "$#" != 0 &&
		die "previous rebase directory $dotest still exists but mbox given."

	test -f "$dotest/interactive" &&
		interactive=_i action=$(cat "$dotest/interactive")

	case "$skip,$abort,$resolved" in
	t*t*)
		die "Please make up your mind. --skip, --resolved or --abort?"
		;;
	t,,)
		run_sequencer$interactive --skip
		;;
	,t,)
		run_sequencer$interactive --abort
		;;
	,,t)
		run_sequencer$interactive --continue
		;;
	,,)
		# No file input but without resume parameters; catch
		# user error to feed us a patch from standard input
		# when there is already $dotest.  This is somewhat
		# unreliable -- stdin could be /dev/null for example
		# and the caller did not intend to feed us a patch but
		# wanted to continue unattended.
		tty -s
		;;
	esac

	die "$dotest still exists. Use git am --abort/--skip/--resolved."
fi

# Make sure we are not given --skip, --resolved, nor --abort
test -z "$abort$resolved$skip" ||
	die 'git-am is not in progress. You cannot use --abort/--skip/--resolved then.'

# sequencer running?
git sequencer --status >/dev/null 2>&1 &&
	die "Sequencer already started. Cannot run git-am."

# Start afresh.
mkdir -p "$dotest" ||
	die "Could not create $dotest directory."

if test -n "$prefix" && test $# != 0
then
	first=t
	for arg
	do
		test -n "$first" && {
			set x
			first=
		}
		case "$arg" in
		/*)
			set "$@" "$arg" ;;
		*)
			set "$@" "$prefix$arg" ;;
		esac
	done
	shift
fi
last=$(git mailsplit -d"$prec" -o"$dotest" -b -- "$@") ||  {
	cleanup
	exit 1
}
this=1

files=$(git diff-index --cached --name-only HEAD --) || exit
if [ "$files" ]; then
	echo "Dirty index: cannot apply patches (dirty: $files)" >&2
	exit 1
fi

test -n "$interactive" && echo 'again' >"$dotest/interactive"

# converting our options to git-sequencer file insn options
test -n "$utf8" || opts="$opts -n"
test -n "$keep" && opts="$opts -k"
test -n "$sign" && opts="$opts --signoff"
test -n "$threeway" && opts="$opts -3"

# these files are created for tab completion scripts
if test -n "$rebasing"
then
	: >"$dotest/rebasing"
else
	: >"$dotest/applying"
	git update-ref ORIG_HEAD HEAD
fi

# create todofile
: > "$todofile" ||
	die_abort "Cannot create $todofile"
while test "$this" -le "$last"
do
	msgnum=$(printf "%0${prec}d" $this)
	this=$(($this+1))

	# This ignores every mail that does not contain a patch.
	grep '^diff' "$dotest/$msgnum" >/dev/null ||
		continue

	extra=
	test -n "$rebasing" &&
		commit=$(sed -e 's/^From \([0-9a-f]*\) .*/\1/' \
			-e q "$dotest/$msgnum") &&
		test "$(git cat-file -t "$commit")" = commit &&
		extra=" -C $commit"

	subject=$(sed -n '1,/^Subject:/s/Subject: *\(\[.*\]\)\{0,1\} *//p' \
		<"$dotest/$msgnum")
	test -n "$GIT_QUIET" -o -n "$interactive" ||
		printf 'run -- printf "Applying: %%s\\\\n" "%s"\n' \
			"$(escape_double_quotes "$subject")" >>"$todofile"
	printf 'patch%s%s "%s" # %s\n' "$opts" "$extra" \
		"$(escape_double_quotes "$dotest/$msgnum")" \
		"$subject" >>"$todofile"
	test -z "$interactive" || echo 'pause' >>"$todofile"
done

seqopts="--no-advice --allow-dirty"
run_sequencer$interactive $GIT_QUIET "$todofile"
